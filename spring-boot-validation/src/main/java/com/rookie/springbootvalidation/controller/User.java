package com.rookie.springbootvalidation.controller;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class User {

    public interface Insert {
    }

    public interface Update {
    }

    @NotEmpty(groups = {Update.class}, message = "Id不能为空")
    private String id;

    @NotEmpty(groups = {Insert.class, Update.class}, message = "用户名不能为空")
    @Size(groups = {Insert.class, Update.class}, min = 5, max = 16, message = "用户名要求5-16位")
    private String uName;

    @NotEmpty(groups = {Insert.class}, message = "密码不能为空")
    private String uPwd;

    @NotEmpty(groups = {Insert.class, Update.class}, message = "邮箱不能为空")
    @Email(groups = {Insert.class, Update.class}, message = "邮箱格式不对")
    private String uEmail;

    @NotEmpty(groups = {Insert.class, Update.class}, message = "手机号不能为空")
    @Size(groups = {Insert.class, Update.class}, min = 11, max = 11, message = "手机号为11位有效手机号")
    private String uTel;

    @NotEmpty(groups = {Insert.class, Update.class}, message = "角色不能为空")
    private String uRank;
}
