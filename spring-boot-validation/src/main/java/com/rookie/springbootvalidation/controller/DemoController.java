package com.rookie.springbootvalidation.controller;

import com.rookie.springbootvalidation.base.R;
import com.rookie.springbootvalidation.dto.ValidatorDTO;
import com.rookie.springbootvalidation.exception.ExceptionCode;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Objects;

@RestController
@Validated
public class DemoController {

    /**
     * 对 @RequestParam 注解修饰的参数（name、id）进行校验
     * 对于这样的情形，要想让注解生效，除了在参数上加注解以外，
     * 例如：@NotEmpty、@Min，同时还需要在类上增加 @Validated 注解
     */
    @GetMapping("detail")
    public R<String> param(@NotBlank(message = "参数不能为空") String id) {
        return R.success(id);
    }

    /**
     * 对 @RequestBody 注解修饰的对象（validatorDTO）进行校验
     * 对于这样的情形，首选需要在接口定义那里对参数（validatorDTO）加上 @Validated 注解
     */
    @PostMapping("add")
    public R<ValidatorDTO> add(@RequestBody @Validated ValidatorDTO validatorDTO) {
        return R.success(validatorDTO);
    }

    /**
     * 对 @RequestBody 注解修饰的 List（List<ValidatorDTO> ） 对象进行校验
     * 该情形与上边介绍的第二种情形，主要是在接口定义那里的注解有区别，
     * 第二种情形使用的是 @Validated 注解，而这里需要使用 @Valid 注解
     * （如果使用 @Validated 注解，则 List 中的 UserDto 里的注解（@NotEmpty、@Min 等等）将全都失效！！！！ ）
     */
    @PostMapping("list")
    public R<List<ValidatorDTO>> list(@RequestBody @Valid List<ValidatorDTO> validatorList) {
        return R.success(validatorList);
    }

    /**
     * JavaBean参数校验（form-data）
     */
    @PostMapping("formAdd")
    public R<ValidatorDTO> formAdd(@Validated ValidatorDTO validatorDTO) {
        return R.success(validatorDTO);
    }

    /**
     *
     * BindingResult result 一定要跟在 @Validated 注解对象的后面,且当有多个@Validated
     * 注解时,每个注解对象后面都需要添加一个
     */
    @PostMapping("bind")
    public R<ValidatorDTO> bind(@Validated ValidatorDTO validatorDTO, BindingResult result) {
        if(result.hasErrors()) {
            return R.fail(Objects.requireNonNull(result.getFieldError()).getDefaultMessage(), ExceptionCode.BASE_VALID_PARAM);
        }
        return R.success(validatorDTO);
    }

    @PostMapping("addUser")
    public R<User> add(@RequestBody @Validated({User.Update.class}) User user) {
        return R.success(user);
    }
}
