package com.rookie.springbootvalidation.dto;

import lombok.Data;

import javax.validation.constraints.*;
import java.io.Serializable;

@Data
public class ValidatorDTO implements Serializable {

    private static final long serialVersionUID = -8442930023168057347L;

    @NotEmpty(message = "姓名不能为空")
    private String name;

    @Min(value = 18, message = "年龄需要大于18")
    @Max(value = 25, message = "年龄需要小于25")
    @NotNull(message = "年龄不能为空")
    private Integer age;

    @Email(message = "请输入正确的邮箱格式")
    @NotEmpty(message = "邮箱不能为空")
    private String email;

}
