package com.rookie.springbootvalidation.handler;

import com.rookie.springbootvalidation.base.R;
import com.rookie.springbootvalidation.exception.BusinessException;
import com.rookie.springbootvalidation.exception.ExceptionCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    /**
     * 业务异常
     *
     * @param ex
     * @param request
     * @return
     */
    @ExceptionHandler(BusinessException.class)
    public R<String> bizException(BusinessException ex, HttpServletRequest request) {
        log.warn("BusinessException:", ex);
        return R.result(ex.getCode(), "", ex.getMessage()).setPath(request.getRequestURI());
    }

    /**
     * 空指针异常
     *
     * @param ex
     * @param request
     * @return
     */
    @ExceptionHandler(NullPointerException.class)
    public R<String> nullPointerException(NullPointerException ex, HttpServletRequest request) {
        log.warn("NullPointerException:", ex);
        return R.result(ExceptionCode.NULL_POINT_EX.getCode(), "", ExceptionCode.NULL_POINT_EX.getMsg()).setPath(request.getRequestURI());
    }

    /**
     * 无效参数异常
     *
     * @param ex
     * @param request
     * @return
     */
    @ExceptionHandler(IllegalArgumentException.class)
    public R<String> illegalArgumentException(IllegalArgumentException ex, HttpServletRequest request) {
        log.warn("IllegalArgumentException:", ex);
        return R.result(ExceptionCode.ILLEGAL_ARGUMENT_EX.getCode(), "", ExceptionCode.ILLEGAL_ARGUMENT_EX.getMsg()).setPath(request.getRequestURI());
    }

    /**
     * ConstraintViolationException (@NotBlank @NotNull @NotEmpty) :
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(ConstraintViolationException.class)
    public R<String> constraintViolationException(ConstraintViolationException ex, HttpServletRequest request) {
        log.warn("ConstraintViolationException:", ex);
        String msg = ex.getConstraintViolations().stream()
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.joining(","));
        return R.result(ExceptionCode.BASE_VALID_PARAM.getCode(), "",
                StringUtils.isEmpty(msg) ? ExceptionCode.BASE_VALID_PARAM.getMsg() : msg).setPath(request.getRequestURI());
    }

    /**
     * BindException (@Valated@Valid 仅对于表单提交有效，对于以 json 格式提交将会失效) :
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(BindException.class)
    public R<String> bindException(BindException ex, HttpServletRequest request) {
        log.warn("BindException:", ex);
        String msg = ex.getBindingResult().getAllErrors().stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.joining(","));
        return R.result(ExceptionCode.BASE_VALID_PARAM.getCode(), "",
                StringUtils.isEmpty(msg) ? ExceptionCode.BASE_VALID_PARAM.getMsg() : msg).setPath(request.getRequestURI());
    }

    /**
     * MethodArgumentNotValidException (@Valated@Valid 前端提交的方式为 json 格式有效，出现异常时会被该异常类处理) :
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public R<String> methodArgumentNotValidException(MethodArgumentNotValidException ex, HttpServletRequest request) {
        log.warn("MethodArgumentNotValidException:", ex);
        String msg = ex.getBindingResult().getAllErrors().stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.joining(","));
        return R.result(ExceptionCode.BASE_VALID_PARAM.getCode(), "",
                StringUtils.isEmpty(msg) ? ExceptionCode.BASE_VALID_PARAM.getMsg() : msg).setPath(request.getRequestURI());
    }

    /**
     * 运行SQL出现异常
     *
     * @param ex
     * @param request
     * @return
     */
    @ExceptionHandler(SQLException.class)
    public R<String> sqlException(SQLException ex, HttpServletRequest request) {
        log.warn("SQLException:", ex);
        return R.result(ExceptionCode.SQL_EX.getCode(), "", ExceptionCode.SQL_EX.getMsg()).setPath(request.getRequestURI());
    }

    /**
     * 其他异常
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(Exception.class)
    public R<String> otherExceptionHandler(Exception ex, HttpServletRequest request) {
        log.warn("Exception:", ex);
        if (ex.getCause() instanceof BusinessException) {
            return this.bizException((BusinessException) ex.getCause(), request);
        }
        return R.result(ExceptionCode.SYSTEM_BUSY.getCode(), "", ExceptionCode.SYSTEM_BUSY.getMsg()).setPath(request.getRequestURI());
    }
}
