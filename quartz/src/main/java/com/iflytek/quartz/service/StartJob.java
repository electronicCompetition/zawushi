package com.iflytek.quartz.service;

import org.quartz.SchedulerException;

public interface StartJob {

    void startJob() throws SchedulerException;
}
