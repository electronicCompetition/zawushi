package com.iflytek.quartz.service.impl;

import com.iflytek.quartz.service.StartJob;
import com.iflytek.quartz.taskJob.QuartzUtil;
import com.iflytek.quartz.taskJob.TaskJobDetail;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.stereotype.Service;

@Service
public class StartJobImpl implements StartJob {

    @Override
    public void startJob() throws SchedulerException {

        // 获取scheduler实例
        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

        // JobDetail 是具体Job实例
        JobDetail jobDetail = JobBuilder.newJob(TaskJobDetail.class).withIdentity("job", "jobGroup").build();

        //corn表达式  每1秒执行一次
        CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule("0/1 * * * * ?");
        //设置定时任务的时间触发规则
        CronTrigger trigger = TriggerBuilder.newTrigger().withIdentity("trigger", "triggerGroup").withSchedule(cronScheduleBuilder).build();

        // 注册任务和定时器
        scheduler.scheduleJob(jobDetail, trigger);
        // 启动 调度器
        scheduler.start();
    }


    public static void main(String[] args) throws SchedulerException, InterruptedException {
        QuartzUtil.addJob(TaskJobDetail.class, "job", "jobGroup", "trigger", "triggerGroup", "0/1 * * * * ?");
//        QuartzUtil.addJob(TaskJobDetailSecond.class, "job1", "jobGroup1", "trigger1", "triggerGroup1", "0/1 * * * * ?");

//        Thread.sleep(4000);
//        QuartzUtil.deleteJob("job1", "jobGroup1");
        Thread.sleep(4000);
        QuartzUtil.modifyJobTime("trigger", "triggerGroup", "0/10 * * * * ?");
    }
}
