package com.iflytek.quartz.taskJob;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

public class QuartzUtil {

    private static Scheduler scheduler;

    static {
        try {
            scheduler = StdSchedulerFactory.getDefaultScheduler();
        } catch (SchedulerException e) {
        }
    }

    public static void addJob(Class jobClazz, String jobName, String jobGroup, String triggerName, String triggerGroup, String cron) throws SchedulerException {
        // 任务详情实例 JobDetail
        JobDetail jobDetail = JobBuilder.newJob(jobClazz)
                .withIdentity(jobName, jobGroup)
                .build();
        //设置定时任务的时间触发规则 trigger
        CronTrigger trigger = TriggerBuilder.newTrigger()
                .withIdentity(triggerName, triggerGroup)
                .withSchedule(CronScheduleBuilder.cronSchedule(cron))
                .build();

        // 注册触发器和任务
        scheduler.scheduleJob(jobDetail, trigger);
        // 启动 调度器
        scheduler.start();
    }

    /**
     * 修改某一任务的执行时间
     *
     *      TriggerBuilder :
     *      public TriggerBuilder<T> withIdentity(String name, String group) {
     *         key = new TriggerKey(name, group);
     *         return this;
     *     }
     *
     * @param triggerName
     * @param triggerGroup
     * @param cron
     * @throws SchedulerException
     */
    public static void modifyJobTime(String triggerName, String triggerGroup, String cron) throws SchedulerException {
        // TriggerKey是表明trigger身份的一个对象
        TriggerKey triggerKey = new TriggerKey(triggerName, triggerGroup);
        CronTrigger trigger = (CronTrigger) scheduler.getTrigger(triggerKey);
        if (trigger == null) {
            return;
        }

        String oldCron = trigger.getCronExpression();
        if (!oldCron.equalsIgnoreCase(cron)) {
            trigger = TriggerBuilder.newTrigger().withIdentity(triggerName, triggerGroup).withSchedule(CronScheduleBuilder.cronSchedule(cron)).build();
            scheduler.rescheduleJob(triggerKey, trigger);
        }
    }


    /**
     * 暂停所有任务
     *
     * @throws SchedulerException
     */
    public static void pauseAllJob() throws SchedulerException {
        scheduler.pauseAll();
    }

    /**
     * 暂停某一个任务
     *
     *     JobBuilder :
     *     public JobBuilder withIdentity(String name, String group) {
     *         key = new JobKey(name, group);
     *         return this;
     *     }
     *
     * @param jobName
     * @param jobGroup
     */
    public static void pauseJob(String jobName, String jobGroup) throws SchedulerException {
        // JobKey是表明Job身份的一个对象
        JobKey jobKey = new JobKey(jobName, jobGroup);
        JobDetail jobDetail = scheduler.getJobDetail(jobKey);
        if (jobDetail == null) {
            return;
        }
        scheduler.pauseJob(jobKey);
    }

    /**
     * 恢复全部暂停任务
     *
     * @throws SchedulerException
     */
    public static void resumeAllJob() throws SchedulerException {
        scheduler.resumeAll();
    }

    /**
     * 恢复某一个暂停任务
     *
     * @param jobName
     * @param jobGroup
     * @throws SchedulerException
     */
    public static void resumeJob(String jobName, String jobGroup) throws SchedulerException {
        JobKey jobKey = new JobKey(jobName, jobGroup);
        JobDetail jobDetail = scheduler.getJobDetail(jobKey);
        if (jobDetail == null) {
            return;
        }
        scheduler.resumeJob(jobKey);
    }

    /**
     * 删除某个任务
     *
     * @param jobName
     * @param jobGroup
     * @throws SchedulerException
     */
    public static void deleteJob(String jobName, String jobGroup) throws SchedulerException {
        JobKey jobKey = new JobKey(jobName, jobGroup);
        JobDetail jobDetail = scheduler.getJobDetail(jobKey);
        if (jobDetail == null) {
            return;
        }
        scheduler.deleteJob(jobKey);
    }
}
