package com.rookie.logback.controller;

import com.rookie.logback.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@RestController
public class TestController {

    /**
     * POST 方式接口测试
     * @param user
     * @return
     */
    @PostMapping("/user")
    public User testPost(@RequestBody User user) {
        log.info("testPost ...");
        return user;
    }

    /**
     * GET 方式接口测试
     * @return
     */
    @GetMapping("/user")
    public String testGet(@RequestParam("username") String username,
                          @RequestParam("password") String password) {
        log.info("testGet ...");
        return "success";
    }

    /**
     * 单文件上传接口测试
     * @return
     */
    @PostMapping("/file/upload")
    public String testFileUpload(@RequestParam("file") MultipartFile file) {
        log.info("testFileUpload ...");
        return "success";
    }

    /**
     * 多文件上传接口测试
     * @return
     */
    @PostMapping("/multiFile/upload")
    public String testMultiFileUpload(@RequestParam("file") MultipartFile[] file) {
        log.info("testMultiFileUpload ...");
        return "success";
    }
}
