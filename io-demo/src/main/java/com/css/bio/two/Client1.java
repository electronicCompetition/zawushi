package com.css.bio.two;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client1 {
    public static void main(String[] args) {
        try {
            Socket socket = new Socket("127.0.0.1", 8089);
            PrintWriter pw = new PrintWriter(socket.getOutputStream());
            Scanner scanner = new Scanner(System.in);
            while (true) {
                System.out.println("say:");
                pw.print(scanner.nextLine());
                pw.flush();
                pw.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
