package com.css.bio.one;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) {
        try {
            //1.请求与服务端的Socket对象连接
            Socket socket = new Socket("127.0.0.1", 8089);
            //2. 得到一个打印流
            PrintWriter pw = new PrintWriter(socket.getOutputStream());
            //3. 使用循环不断的发送消息给服务端接收
            Scanner scanner = new Scanner(System.in);
            while (true) {
                System.out.println("say:");
                pw.print(scanner.nextLine());
                pw.flush();
                pw.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
