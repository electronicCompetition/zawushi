package com.css.bio.one;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    public static void main(String[] args) {
        try {
            // 同步并阻塞（传统阻塞型），服务器实现模式为一个连接一个线程，即客户端有连接请求时服务器 端就
            // 需要启动一个线程进行处理，如果这个连接不做任何事情会造成不必要的线程开销
            System.out.println("===服务端启动===");
            //1.定义一个ServerSocket对象进行服务端的端口注册
            ServerSocket serverSocket = new ServerSocket(8089);
            //2. 监听客户端的Socket连接请求
            Socket accept = serverSocket.accept();
            //3.从socket管道中得到一个字节输入流对象
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(accept.getInputStream()));
            String msg = null;
            if ((msg = bufferedReader.readLine()) != null) {
                System.out.println("Server: " + msg);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
