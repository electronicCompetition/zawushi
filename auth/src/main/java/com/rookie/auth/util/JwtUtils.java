package com.rookie.auth.util;

import com.rookie.auth.common.ExceptionEnum;
import com.rookie.auth.common.exception.BusinessException;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

@Component
@Slf4j
public class JwtUtils {

    @Value("${authentication.secret}")
    private String secret;

    @Value("${authentication.expire}")
    private long expire;

    /**
     * 获取token
     * @param userInfo
     * @return
     */
    public String getToken(JwtUserInfo userInfo) {
        // jwt header
        HashMap<String, Object> header = new HashMap<>();
        header.put("alg", SignatureAlgorithm.HS256.getValue());
        header.put("type", "JWT");

        // jwt payload
        HashMap<String, Object> body = new HashMap<>();
        body.put("userId", userInfo.getUserId());
        body.put("account", userInfo.getAccount());
        body.put("name", userInfo.getName());
        body.put("orgId", userInfo.getOrgId());
        body.put("departmentId", userInfo.getStationId());

        //过期时间
        Date nowDate = new Date();
        Date expireDate = new Date(nowDate.getTime() + expire * 1000);
        // 获取token
        return Jwts.builder().setHeader(header)
                // 设置jti(JWT ID)：是JWT的唯一标识，根据业务需要，这个可以设置为一个不重复的值，主要用来作为一次性token,从而回避重放攻击。
                .setId(UUID.randomUUID().toString())
                .setClaims(body)
                // iat: jwt的签发时间
                .setIssuedAt(nowDate)
                .setExpiration(expireDate)
                // 设置签名使用的签名算法和签名使用的秘钥
                .signWith(SignatureAlgorithm.HS256, secret)
                .compact();
    }

    /**
     * 解析token
     * @param token
     * @return
     */
    public Jws<Claims> parseToken(String token) {
        try {
            return Jwts.parser().setSigningKey(secret).parseClaimsJws(token);
        } catch (ExpiredJwtException ex) {
            //过期
            throw new BusinessException(ExceptionEnum.JWT_TOKEN_EXPIRED.getCode(), ExceptionEnum.JWT_TOKEN_EXPIRED.getMsg());
        } catch (SignatureException ex) {
            //签名错误
            throw new BusinessException(ExceptionEnum.JWT_SIGNATURE.getCode(), ExceptionEnum.JWT_SIGNATURE.getMsg());
        } catch (IllegalArgumentException ex) {
            //token 为空
            throw new BusinessException(ExceptionEnum.JWT_ILLEGAL_ARGUMENT.getCode(), ExceptionEnum.JWT_ILLEGAL_ARGUMENT.getMsg());
        } catch (Exception e) {
            log.error("errCode:{}, message:{}", ExceptionEnum.JWT_PARSER_TOKEN_FAIL.getCode(), e.getMessage());
            throw new BusinessException(ExceptionEnum.JWT_PARSER_TOKEN_FAIL.getCode(), ExceptionEnum.JWT_PARSER_TOKEN_FAIL.getMsg());
        }
    }

    /**
     * 获取token中的用户信息
     * @param token
     * @return
     */
    public JwtUserInfo getInfoFromToken(String token) {
        Claims body = parseToken(token).getBody();
        return new JwtUserInfo(body.get("userId", String.class),
                body.get("account", String.class),
                body.get("name", String.class),
                body.get("orgId", String.class),
                body.get("departmentId", String.class));
    }
}
