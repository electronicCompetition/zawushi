package com.rookie.auth.entity;

import lombok.Data;

import java.util.Date;

@Data
public class AuthUser {

    private String id;

    private String account;

    private String name;

    private String orgId;

    private String stationId;

    private String email;

    private String mobile;

    private String sex;

    private Boolean status;

    private String avatar;

    private String workDescribe;

    private Date passwordErrorLastTime;

    private Integer passwordErrorNum;

    private Date passwordExpireTime;

    private String password;

    private Date lastLoginTime;

    private String createUser;

    private Date createTime;

    private String updateUser;

    private Date updateTime;

}