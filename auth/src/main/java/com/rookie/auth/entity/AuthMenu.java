package com.rookie.auth.entity;

import lombok.Data;

import java.util.Date;

@Data
public class AuthMenu {
    private String id;

    private String name;

    private String description;

    private Boolean isPublic;

    private String path;

    private String component;

    private Boolean isEnable;

    private Integer sortValue;

    private String icon;

    private String group;

    private String parentId;

    private String createUser;

    private Date createTime;

    private String updateUser;

    private Date updateTime;
}