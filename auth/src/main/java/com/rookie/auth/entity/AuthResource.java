package com.rookie.auth.entity;

import lombok.Data;

import java.util.Date;

@Data
public class AuthResource {

    private String id;

    private String code;

    private String name;

    private String menuId;

    private String method;

    private String url;

    private String description;

    private String createUser;

    private Date createTime;

    private String updateUser;

    private Date updateTime;

}