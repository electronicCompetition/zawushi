package com.rookie.auth.entity;

import lombok.Data;

import java.util.List;

@Data
public class RegionTree {

    private String regionCode;

    private String regionName;

    private String parentId;

    private List<RegionTree> nodes;
}
