package com.rookie.auth.cocnfig;

import com.rookie.auth.interceptor.AuthInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfigurer implements WebMvcConfigurer {

    @Autowired
    private AuthInterceptor authInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(authInterceptor)
                .addPathPatterns("/**")          // 所有路径都被拦截
                .excludePathPatterns(            // 添加不拦截路径
                        "/**/*.html",            // html静态资源
                        "/**/*.js",              // js静态资源
                        "/**/*.css",             // css静态资源
                        "/**/*.woff",
                        "/**/*.ttf",
                        "/**/webjars/**",
                        "/**/v2/api-docs/**",
                        "/**/v2/api-docs-ext/**",
                        "/**/swagger-resources/**",
                        "/auth/**",
                        "/attachment/**",
                        "/dict/**"
                );
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowedMethods("POST", "GET", "PUT", "OPTIONS", "DELETE") // 允许任何方法（post、get等）
                .allowedHeaders("*") // 允许任何请求头
                .allowCredentials(true) // 允许证书、cookie
                .exposedHeaders(HttpHeaders.SET_COOKIE)
                .maxAge(3600L); // maxAge(3600)表明在3600秒内，不需要再发送预检验请求，可以缓存该结果
    }

}
