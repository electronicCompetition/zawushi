package com.rookie.auth.cocnfig;

import com.rookie.auth.common.DictCacheManager;
import com.rookie.auth.dao.DictDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@Order(1)
@Slf4j
public class DictCacheRunner implements ApplicationRunner {

    @Autowired
    private DictDao dictDao;

    @Autowired
    private DictCacheManager dictCacheManager;

    @Override
    public void run(ApplicationArguments applicationArguments) {
        log.info("=======================================");
        log.info("               初始化缓存字典项           ");
        log.info("=======================================");
        // 刷新缓存字典，重新把缓存字典表的数据刷新到缓存管理器中
        List<Map<String, String>> maps = dictDao.queryCacheDict();
        if (!CollectionUtils.isEmpty(maps)) {
            Map<String, List<Map<String, String>>> map = maps.stream().collect(Collectors.groupingBy(e -> e.get("dictType")));
            for (List<Map<String, String>> value : map.values()) {
                for (Map<String, String> v : value) {
                    v.remove("dictType");
                }
            }

            dictCacheManager.addAdd(map);
        }
    }
}