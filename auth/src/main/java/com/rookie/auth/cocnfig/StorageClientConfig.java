package com.rookie.auth.cocnfig;

import org.csource.common.MyException;
import org.csource.fastdfs.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@Configuration
public class StorageClientConfig {

    @Autowired
    private FastDFSConfig fastDFSConfig;

    @Bean
    public StorageClient storageClient() throws IOException, MyException {
        this.initClientGlobal();

        TrackerClient trackerClient = new TrackerClient();
        TrackerServer trackerServer = trackerClient.getConnection();
        StorageServer storageServer = trackerClient.getStoreStorage(trackerServer);

        return new StorageClient(trackerServer, storageServer);
    }

    private void initClientGlobal() throws IOException, MyException {
        ClientGlobal.initByTrackers(fastDFSConfig.getTrackerServer());

        if (fastDFSConfig.getTrackerHttpPort() != null && fastDFSConfig.getTrackerHttpPort() != 0) {
            ClientGlobal.setG_tracker_http_port(fastDFSConfig.getTrackerHttpPort());
        }

        if (fastDFSConfig.getConnectTimeout() != null && fastDFSConfig.getConnectTimeout() != 0) {
            ClientGlobal.setG_connect_timeout(fastDFSConfig.getConnectTimeout());
        }
        if (fastDFSConfig.getNetworkTimeout() != null && fastDFSConfig.getNetworkTimeout() != 0) {
            ClientGlobal.setG_network_timeout(fastDFSConfig.getNetworkTimeout());
        }
    }

}
