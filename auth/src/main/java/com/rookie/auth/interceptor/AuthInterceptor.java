package com.rookie.auth.interceptor;

import com.alibaba.fastjson.JSON;
import com.rookie.auth.common.base.ApiResponse;
import com.rookie.auth.entity.AuthResource;
import com.rookie.auth.service.ResourceService;
import com.rookie.auth.util.JwtUtils;
import com.rookie.auth.util.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
public class AuthInterceptor implements HandlerInterceptor {

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private ResourceService resourceService;

    @Autowired
    private JwtUtils jwtUtils;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        // 第1步：获取当前请求的请求方式和uri，拼接成GET/user/page这种形式，称为权限标识符
        String method = request.getMethod();// GET POST PUT
        String uri = request.getRequestURI();
        String permission = method + uri; // GET/user/page

        /*
         * 第2步：从缓存中获取所有需要进行鉴权的资源(同样是由资源表的method字段值+url字段值拼接成)，
         * 如果没有获取到则调用权限服务获取并放入缓存中
         */
        List<String> list = (List<String>) redisUtils.get("global_resource");
        if (CollectionUtils.isEmpty(list)) {
            // 缓存中没有相应数据 放入缓存中
            list = resourceService.list();
            redisUtils.set("global_resource", list);
        }

        // 第3步：判断这些资源是否包含当前请求的权限标识符，如果不包含当前请求的权限标识符，则返回未经授权错误提示
        if (!list.contains(permission)) {
            return errorResponse(response, "未经授权！获取资源失败", HttpStatus.UNAUTHORIZED);
        }

        /*
         * 第4步：如果包含当前的权限标识符，则从header中取出用户id，根据用户id取出缓存中的用户拥有的权限，
         * 如果没有取到则通过调用权限服务获取并放入缓存，判断用户拥有的权限是否包含当前请求的权限标识符
         */
        String token = request.getHeader("token");
        String userId = jwtUtils.getInfoFromToken(token).getUserId();
        List<String> visibleResource = (List<String>) redisUtils.get(userId);
        if (CollectionUtils.isEmpty(visibleResource)) {
            // 缓存中不存在，需要通过调用权限服务来获取
            List<AuthResource> resourceList = resourceService.queryPermissions(userId);
            if (!CollectionUtils.isEmpty(visibleResource)) {
                visibleResource = resourceList.stream().map((e -> e.getMethod() + e.getUrl())).collect(Collectors.toList());
                // 将当前用户拥有的权限载入缓存
                redisUtils.set(userId, visibleResource);
            }
        }

        // 第5步：如果用户拥有的权限包含当前请求的权限标识符则说明当前用户拥有权限，直接放行
        if (CollectionUtils.isEmpty(visibleResource) || !visibleResource.contains(permission)) {
            // 第6步：如果用户拥有的权限不包含当前请求的权限标识符则说明当前用户没有权限，返回未经授权错误提示
            return errorResponse(response, "未经授权！获取资源失败", HttpStatus.UNAUTHORIZED);
        }

        return true;
    }

    /**
     * 直接返回到前端
     *
     * @param response   response
     * @param errMsg     errMsg
     * @param httpStatus httpStatus
     * @return
     */
    private boolean errorResponse(HttpServletResponse response, String errMsg, HttpStatus httpStatus) {
        response.setCharacterEncoding("UTF-8");
        response.setStatus(httpStatus.value());
        response.setContentType("application/json;charset=UTF-8");

        try (PrintWriter pw = response.getWriter()) {
            pw.write(JSON.toJSONString(ApiResponse.fail(-1, errMsg)));
            pw.flush();
        } catch (IOException e) {
            log.error(e.getMessage());
        }

        return false;
    }
}
