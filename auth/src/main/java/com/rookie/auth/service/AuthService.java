package com.rookie.auth.service;

import com.rookie.auth.common.base.ApiResponse;
import com.rookie.auth.vo.LoginVo;

public interface AuthService {

    /**
     * 用户登录
     * @param account 账号
     * @param password 密码
     * @return LoginVo
     */
    ApiResponse<LoginVo> login(String account, String password);
}
