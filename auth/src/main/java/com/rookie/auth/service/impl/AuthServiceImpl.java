package com.rookie.auth.service.impl;

import com.rookie.auth.common.ExceptionEnum;
import com.rookie.auth.common.base.ApiResponse;
import com.rookie.auth.dao.AuthDao;
import com.rookie.auth.entity.AuthResource;
import com.rookie.auth.entity.AuthUser;
import com.rookie.auth.service.AuthService;
import com.rookie.auth.service.ResourceService;
import com.rookie.auth.util.JwtUserInfo;
import com.rookie.auth.util.JwtUtils;
import com.rookie.auth.util.RedisUtils;
import com.rookie.auth.vo.LoginVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.DigestUtils;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AuthServiceImpl implements AuthService {

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private AuthDao authDao;

    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private ResourceService resourceService;

    @Override
    public ApiResponse<LoginVo> login(String account, String password) {
        // 检验账号、密码是否正确
        AuthUser user = authDao.queryByAccount(account);
        if (user == null || !StringUtils.equals(user.getPassword(), DigestUtils.md5DigestAsHex(password.getBytes()))) {
            return ApiResponse.fail(ExceptionEnum.JWT_USER_INVALID.getCode(), ExceptionEnum.JWT_USER_INVALID.getMsg());
        }

        // 用户访问资源权限信息
        List<AuthResource> authResources = resourceService.queryPermissions(user.getId());

        List<String> permissionList = null;
        if (!CollectionUtils.isEmpty(authResources)) {
            // 用户对应的权限（给前端使用的）
            permissionList = authResources.stream().map(AuthResource::getCode).collect(Collectors.toList());

            // 将用户对应的权限进行缓存
            List<String> visibleResource = authResources.stream().map(e -> e.getMethod() + e.getUrl()).collect(Collectors.toList());
            // 将用户对应的权限进行缓存
            redisUtils.set(user.getId(), visibleResource);
        }

        // 生成token
        JwtUserInfo jwtUserInfo = new JwtUserInfo();
        jwtUserInfo.setUserId(user.getId());
        BeanUtils.copyProperties(user, jwtUserInfo);
        LoginVo loginVo = LoginVo.builder()
                .user(user)
                .token(jwtUtils.getToken(jwtUserInfo))
                .permissionList(permissionList)
                .build();

        return ApiResponse.success(loginVo);

    }

}
