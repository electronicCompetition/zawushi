package com.rookie.auth.service.impl;

import com.rookie.auth.common.exception.BusinessException;
import com.rookie.auth.service.ValidateCodeService;
import com.rookie.auth.util.RedisUtils;
import com.wf.captcha.ArithmeticCaptcha;
import com.wf.captcha.base.Captcha;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Service
public class ValidateCodeServiceImpl implements ValidateCodeService {

    @Autowired
    private RedisUtils redisUtils;

    @Override
    public void create(String key, HttpServletResponse response) throws IOException {
        if (StringUtils.isEmpty(key)) {
            throw new BusinessException("验证码key不能为空！");
        }

        // 生成验证码
        Captcha captcha = new ArithmeticCaptcha();
        String text = captcha.text();
        // 缓存验证码
        redisUtils.set(key, text, 1000L);

        // 将生成的验证码图片通过输出流写回客户端浏览器
        response.setContentType(MediaType.IMAGE_PNG_VALUE);
        response.setHeader(HttpHeaders.PRAGMA, "No-cache");
        response.setHeader(HttpHeaders.CACHE_CONTROL, "No-cache");
        response.setDateHeader(HttpHeaders.EXPIRES, 0L);
        captcha.out(response.getOutputStream());
    }

    @Override
    public boolean check(String key, String code) {
        // 根据key获取缓存中的验证码
        String value = (String) redisUtils.get(key);
        if (StringUtils.isBlank(value)) {
            throw new BusinessException("验证码已经过期！");
        } else if (!StringUtils.equals(value, code)) {
            throw new BusinessException("验证码错误！");
        }
        // 验证码验证成功后立即清除验证码
        redisUtils.del(key);
        return true;
    }
}
