package com.rookie.auth.service.impl;

import com.rookie.auth.dao.AttachmentDao;
import com.rookie.auth.entity.Attachment;
import com.rookie.auth.service.AttachmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AttachmentServiceImpl implements AttachmentService {

    @Autowired
    private AttachmentDao attachmentDao;

    @Override
    public void saveAttachment(Attachment attachment) {
        attachmentDao.insert(attachment);
    }

    @Override
    public Attachment queryAttachmentById(String id) {
        return attachmentDao.selectByPrimaryKey(id);
    }
}
