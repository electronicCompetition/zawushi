package com.rookie.auth.service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface ValidateCodeService {

    /**
     * 获取登录验证码
     * @param key   验证码的唯一标识
     * @param response response
     * @throws IOException
     */
    void create(String key, HttpServletResponse response) throws IOException;

    /**
     * 校验验证码
     * @param key   验证码的唯一标识
     * @param code  验证码
     * @return boolean
     */
    boolean check(String key, String code);
}
