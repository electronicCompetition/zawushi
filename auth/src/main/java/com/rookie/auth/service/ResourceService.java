package com.rookie.auth.service;

import com.rookie.auth.entity.AuthResource;

import java.util.List;

public interface ResourceService {

    /**
     * 账号拥有资源信息
     * @param userId 用户id
     * @return List<AuthResource>
     */
    List<AuthResource> queryPermissions(String userId);

    /**
     * 查询所有资源
     * @return List<String>
     */
    List<String> list();

}
