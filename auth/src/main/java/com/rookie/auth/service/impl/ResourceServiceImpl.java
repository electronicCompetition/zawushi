package com.rookie.auth.service.impl;

import com.rookie.auth.dao.ResourceDao;
import com.rookie.auth.entity.AuthResource;
import com.rookie.auth.service.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ResourceServiceImpl implements ResourceService {

    @Autowired
    private ResourceDao resourceDao;

    @Override
    public List<AuthResource> queryPermissions(String userId) {
        return resourceDao.queryPermissions(userId);
    }

    @Override
    public List<String> list() {
        List<AuthResource> list = resourceDao.queryAll();
        return list.stream().map(e -> e.getMethod() + e.getUrl()).collect(Collectors.toList());
    }
}
