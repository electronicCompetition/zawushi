package com.rookie.auth.service.impl;

import com.rookie.auth.dao.MenuDao;
import com.rookie.auth.service.MenuService;
import com.rookie.auth.vo.MenuTreeVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MenuServiceImpl implements MenuService {

    @Autowired
    private MenuDao menuDao;

    @Override
    public List<MenuTreeVo> queryUserMenu(String userId) {
        // 查询全部行政区域信息
        List<MenuTreeVo> list = menuDao.queryUserMenu(userId);
        if (!CollectionUtils.isEmpty(list)) {
            // 父节点是null的，为根节点。
            List<MenuTreeVo> tree = list.stream()
                    .filter(e -> StringUtils.isEmpty(e.getParentId()))
                    .collect(Collectors.toList());

            tree.forEach(e -> {
                // 获取根节点下的所有子节点
                List<MenuTreeVo> childList = getChild(e.getId(), list);
                // 给根节点设置子节点
                e.setChildren(childList);
            });
            return tree;
        }
        return null;
    }


    /**
     * 获取子节点
     *
     * @param id
     * @param list
     * @return List<MenuTreeVo>
     */
    private List<MenuTreeVo> getChild(String id, List<MenuTreeVo> list) {
        // 子菜单
        List<MenuTreeVo> childList = new ArrayList<>();
        list.forEach(e -> {
            // 遍历所有节点，将所有菜单的父id与传过来的根节点的id比较
            // 相等说明：为该根节点的子节点。
            if (StringUtils.equals(e.getParentId(), id)) {
                childList.add(e);
            }
        });

        // 递归
        childList.forEach(e -> e.setChildren(getChild(e.getId(), list)));

        // 如果节点下没有子节点，返回一个null（递归退出）
        if (childList.size() == 0) {
            return null;
        }
        return childList;
    }
}
