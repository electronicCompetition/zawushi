package com.rookie.auth.service.impl;

import com.rookie.auth.common.DictCacheManager;
import com.rookie.auth.dao.DictDao;
import com.rookie.auth.entity.RegionTree;
import com.rookie.auth.service.DictService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class DictServiceImpl implements DictService {

    @Autowired
    private DictDao dictDao;

    @Autowired
    private DictCacheManager dictCacheManager;

    @Override
    public List<Map<String, String>> queryDictItem() {
        return dictDao.queryDictItem();
    }

    @Override
    public List<Map<String, String>> queryDict(String dictType) {
        List<Map<String, String>> list = (List<Map<String, String>>) dictCacheManager.get(dictType);
        if (!CollectionUtils.isEmpty(list)) {
            return list;
        }

        return dictDao.queryDict(dictType);
    }

    @Override
    public List<Map<String, String>> queryRegion(String regionCode) {
        return dictDao.queryRegion(regionCode);
    }

    @Override
    public List<RegionTree> queryRegionTree() {
        // 查询全部行政区域信息
        List<RegionTree> list = dictDao.queryRegionAll();
        if (!CollectionUtils.isEmpty(list)) {
            // 父节点是null的，为根节点。
            List<RegionTree> tree = list.stream()
                    .filter(e -> StringUtils.isEmpty(e.getParentId()))
                    .collect(Collectors.toList());

            tree.forEach(e -> {
                // 获取根节点下的所有子节点
                List<RegionTree> childList = getChild(e.getRegionCode(), list);
                // 给根节点设置子节点
                e.setNodes(childList);
            });
            return tree;
        }
        return null;
    }

    /**
     * 获取子节点
     *
     * @param regionCode
     * @param list
     * @return List<RegionTree>
     */
    private List<RegionTree> getChild(String regionCode, List<RegionTree> list) {
        // 子菜单
        List<RegionTree> childList = new ArrayList<>();
        list.forEach(e -> {
            // 遍历所有节点，将所有菜单的父regionCode与传过来的根节点的regionCode比较
            // 相等说明：为该根节点的子节点。
            if (StringUtils.equals(e.getParentId(), regionCode)) {
                childList.add(e);
            }
        });

        // 递归
        childList.forEach(e -> e.setNodes(getChild(e.getRegionCode(), list)));

        // 如果节点下没有子节点，返回一个null（递归退出）
        if (childList.size() == 0) {
            return null;
        }
        return childList;
    }
}
