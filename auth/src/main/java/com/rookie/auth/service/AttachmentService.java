package com.rookie.auth.service;


import com.rookie.auth.entity.Attachment;

public interface AttachmentService {

    void saveAttachment(Attachment attachment);

    Attachment queryAttachmentById(String id);
}
