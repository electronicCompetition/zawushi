package com.rookie.auth.common.handler;

import com.rookie.auth.common.ExceptionEnum;
import com.rookie.auth.common.base.ApiResponse;
import com.rookie.auth.common.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    /**
     * 业务异常
     *
     * @param ex
     * @param request
     * @return
     */
    @ExceptionHandler(BusinessException.class)
    public ApiResponse<String> bizException(BusinessException ex, HttpServletRequest request) {
        log.warn("BusinessException:", ex);
        return ApiResponse.result(ex.getCode(), "", ex.getMessage()).setPath(request.getRequestURI());
    }

    /**
     * 空指针异常
     *
     * @param ex
     * @param request
     * @return
     */
    @ExceptionHandler(NullPointerException.class)
    public ApiResponse<String> nullPointerException(NullPointerException ex, HttpServletRequest request) {
        log.warn("NullPointerException:", ex);
        return ApiResponse.result(ExceptionEnum.NULL_POINT_EX.getCode(), "", ExceptionEnum.NULL_POINT_EX.getMsg()).setPath(request.getRequestURI());
    }

    /**
     * 无效参数异常
     *
     * @param ex
     * @param request
     * @return
     */
    @ExceptionHandler(IllegalArgumentException.class)
    public ApiResponse<String> illegalArgumentException(IllegalArgumentException ex, HttpServletRequest request) {
        log.warn("IllegalArgumentException:", ex);
        return ApiResponse.result(ExceptionEnum.ILLEGAL_ARGUMENT_EX.getCode(), "", ExceptionEnum.ILLEGAL_ARGUMENT_EX.getMsg()).setPath(request.getRequestURI());
    }

    /**
     * 单个参数异常处理
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(ConstraintViolationException.class)
    public ApiResponse<String> constraintViolationException(ConstraintViolationException ex, HttpServletRequest request) {
        log.warn("ConstraintViolationException:", ex);
        Set<ConstraintViolation<?>> constraintViolations = ex.getConstraintViolations();
        if(!CollectionUtils.isEmpty(constraintViolations)) {
            StringBuilder sb = new StringBuilder();
            constraintViolations.forEach(e -> sb.append(e.getMessage()).append(","));
            return ApiResponse.result(ExceptionEnum.BASE_VALID_PARAM.getCode(), "", sb.substring(0, sb.lastIndexOf(","))).setPath(request.getRequestURI());
        }
        return ApiResponse.result(ExceptionEnum.BASE_VALID_PARAM.getCode(), "", ExceptionEnum.BASE_VALID_PARAM.getMsg()).setPath(request.getRequestURI());
    }

    /**
     * JavaBean参数校验（form-data）
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(BindException.class)
    public ApiResponse<String> bindException(BindException ex, HttpServletRequest request) {
        log.warn("BindException:", ex);
        List<ObjectError> allErrors = ex.getBindingResult().getAllErrors();
        if(!CollectionUtils.isEmpty(allErrors)) {
            StringBuilder sb = new StringBuilder();
            allErrors.forEach(e -> sb.append(e.getDefaultMessage()).append(","));
            return ApiResponse.result(ExceptionEnum.BASE_VALID_PARAM.getCode(), "", sb.substring(0, sb.lastIndexOf(","))).setPath(request.getRequestURI());
        }
        return ApiResponse.result(ExceptionEnum.BASE_VALID_PARAM.getCode(), "", ExceptionEnum.BASE_VALID_PARAM.getMsg()).setPath(request.getRequestURI());
    }

    /**
     * JavaBean参数校验（json） 验证异常
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ApiResponse<String> methodArgumentNotValidException(MethodArgumentNotValidException ex, HttpServletRequest request) {
        log.warn("MethodArgumentNotValidException:", ex);
        return ApiResponse.result(ExceptionEnum.BASE_VALID_PARAM.getCode(), "", ex.getBindingResult().getFieldError().getDefaultMessage()).setPath(request.getRequestURI());
    }

    /**
     * 运行SQL出现异常
     *
     * @param ex
     * @param request
     * @return
     */
    @ExceptionHandler(SQLException.class)
    public ApiResponse<String> sqlException(SQLException ex, HttpServletRequest request) {
        log.warn("SQLException:", ex);
        return ApiResponse.result(ExceptionEnum.SQL_EX.getCode(), "", ExceptionEnum.SQL_EX.getMsg()).setPath(request.getRequestURI());
    }

    /**
     * 其他异常
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(Exception.class)
    public ApiResponse<String> otherExceptionHandler(Exception ex, HttpServletRequest request) {
        log.warn("Exception:", ex);
        if (ex.getCause() instanceof BusinessException) {
            return this.bizException((BusinessException) ex.getCause(), request);
        }
        return ApiResponse.result(ExceptionEnum.SYSTEM_BUSY.getCode(), "", ExceptionEnum.SYSTEM_BUSY.getMsg()).setPath(request.getRequestURI());
    }
}
