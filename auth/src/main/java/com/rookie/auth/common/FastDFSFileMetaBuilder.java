package com.rookie.auth.common;

import com.sun.istack.internal.NotNull;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

public class FastDFSFileMetaBuilder {

    /**
     * 根据上传附件构造FastDFSFileMeta
     *
     * @param file
     * @return
     * @throws IOException
     */
    public static FastDFSFileMeta build(@NotNull MultipartFile file) throws IOException {
        FastDFSFileMeta meta = new FastDFSFileMeta();
        meta.setContent(file.getBytes());
        meta.setName(file.getOriginalFilename());
        meta.setExt(Objects.requireNonNull(file.getOriginalFilename()).substring(file.getOriginalFilename().indexOf(".") + 1));
        meta.setFileSize(file.getSize());
        return meta;
    }

    public static FastDFSFileMeta buildBigFile(@NotNull MultipartFile file) {
        FastDFSFileMeta meta = new FastDFSFileMeta();
        meta.setName(file.getOriginalFilename());
        meta.setExt(Objects.requireNonNull(file.getOriginalFilename()).substring(file.getOriginalFilename().indexOf(".") + 1));
        meta.setFileSize(file.getSize());
        return meta;
    }

    public static FastDFSFileMeta build(@NotNull File file) throws IOException {
        FastDFSFileMeta meta = new FastDFSFileMeta();
        meta.setName(file.getName());
        meta.setExt(file.getName().substring(file.getName().indexOf(".") + 1));
        meta.setFileSize(file.length());

        InputStream in = null;
        try {
            in = new FileInputStream(file);
            byte[] bytes = new byte[in.available()];
            in.read(bytes);

            meta.setContent(bytes);
        } finally {
            if (in != null) {
                in.close();
            }
        }
        return meta;
    }

    public static FastDFSFileMeta buildBigFile(@NotNull File file) throws IOException {
        FastDFSFileMeta meta = new FastDFSFileMeta();
        meta.setName(file.getName());
        meta.setExt(file.getName().substring(file.getName().indexOf(".") + 1));
        meta.setFileSize(file.length());
        return meta;
    }
}