package com.rookie.auth.common.base;

import com.rookie.auth.common.exception.BusinessException;
import com.rookie.auth.common.ExceptionEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.HashMap;
import java.util.Map;



@Data
@Accessors(chain = true)
@ApiModel(value = "通用PI接口返回", description = "Common Api Response")
public class ApiResponse<T> {
    public static final String DEF_ERROR_MESSAGE = "系统繁忙，请稍候再试";
    public static final String HYSTRIX_ERROR_MESSAGE = "请求超时，请稍候再试";
    public static final int SUCCESS_CODE = 0;
    public static final int FAIL_CODE = -1;
    public static final int TIMEOUT_CODE = -2;
    /**
     * 统一参数验证异常
     */
    public static final int VALID_EX_CODE = -9;
    public static final int OPERATION_EX_CODE = -10;

    @ApiModelProperty(value = "通用返回状态", required = true)
    private int code;

    @ApiModelProperty(value = "通用返回信息", required = true)
    private String msg = "ok";

    @ApiModelProperty(value = "通用返回数据", required = true)
    private T data;

    @ApiModelProperty(value = "请求路径", required = true)
    private String path;

    @ApiModelProperty(value = "附加数据", required = true)
    private Map<String, Object> extra;

    @ApiModelProperty(value = "响应时间", required = true)
    private long timestamp = System.currentTimeMillis();

    private ApiResponse() {
        super();
    }

    public ApiResponse(int code, T data, String msg) {
        this.code = code;
        this.data = data;
        this.msg = msg;
    }

    public static <E> ApiResponse<E> result(int code, E data, String msg) {
        return new ApiResponse<>(code, data, msg);
    }

    /**
     * 请求成功消息
     *
     * @param data 结果
     * @return RPC调用结果
     */
    public static <E> ApiResponse<E> success(E data) {
        return new ApiResponse<>(SUCCESS_CODE, data, "ok");
    }

    public static ApiResponse<Boolean> success() {
        return new ApiResponse<>(SUCCESS_CODE, true, "ok");
    }

    /**
     * 请求成功方法 ，data返回值，msg提示信息
     *
     * @param data 结果
     * @param msg  消息
     * @return RPC调用结果
     */
    public static <E> ApiResponse<E> success(E data, String msg) {
        return new ApiResponse<>(SUCCESS_CODE, data, msg);
    }

    /**
     * 请求失败消息
     *
     * @param msg
     * @return
     */
    public static <E> ApiResponse<E> fail(int code, String msg) {
        return new ApiResponse<>(code, null, (msg == null || msg.isEmpty()) ? DEF_ERROR_MESSAGE : msg);
    }

    public static <E> ApiResponse<E> fail(String msg) {
        return fail(OPERATION_EX_CODE, msg);
    }

    public static <E> ApiResponse<E> fail(String msg, Object... args) {
        String message = (msg == null || msg.isEmpty()) ? DEF_ERROR_MESSAGE : msg;
        return new ApiResponse<>(OPERATION_EX_CODE, null, String.format(message, args));
    }

    public static <E> ApiResponse<E> fail(ExceptionEnum exceptionEnum) {
        return validFail(exceptionEnum);
    }

    public static <E> ApiResponse<E> fail(BusinessException exception) {
        if (exception == null) {
            return fail(DEF_ERROR_MESSAGE);
        }
        return new ApiResponse<>(exception.getCode(), null, exception.getMessage());
    }

    /**
     * 请求失败消息，根据异常类型，获取不同的提供消息
     *
     * @param throwable 异常
     * @return RPC调用结果
     */
    public static <E> ApiResponse<E> fail(Throwable throwable) {
        return fail(FAIL_CODE, throwable != null ? throwable.getMessage() : DEF_ERROR_MESSAGE);
    }

    public static <E> ApiResponse<E> validFail(String msg) {
        return new ApiResponse<>(VALID_EX_CODE, null, (msg == null || msg.isEmpty()) ? DEF_ERROR_MESSAGE : msg);
    }

    public static <E> ApiResponse<E> validFail(String msg, Object... args) {
        String message = (msg == null || msg.isEmpty()) ? DEF_ERROR_MESSAGE : msg;
        return new ApiResponse<>(VALID_EX_CODE, null, String.format(message, args));
    }

    public static <E> ApiResponse<E> validFail(ExceptionEnum exceptionEnum) {
        return new ApiResponse<>(exceptionEnum.getCode(), null,
                (exceptionEnum.getMsg() == null || exceptionEnum.getMsg().isEmpty()) ? DEF_ERROR_MESSAGE : exceptionEnum.getMsg());
    }

    public static <E> ApiResponse<E> timeout() {
        return fail(TIMEOUT_CODE, HYSTRIX_ERROR_MESSAGE);
    }


    public ApiResponse<T> put(String key, Object value) {
        if (this.extra == null) {
            this.extra = new HashMap<>();
        }
        this.extra.put(key, value);
        return this;
    }

    /**
     * 逻辑处理是否成功
     *
     * @return 是否成功
     */
    public Boolean getIsSuccess() {
        return this.code == SUCCESS_CODE || this.code == 200;
    }

    /**
     * 逻辑处理是否失败
     *
     * @return
     */
    public Boolean getIsError() {
        return !getIsSuccess();
    }

}
