package com.rookie.auth.controller;

import com.rookie.auth.common.base.ApiResponse;
import com.rookie.auth.entity.AuthResource;
import com.rookie.auth.service.ResourceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 前端控制器
 * 资源
 */
@Slf4j
@Validated
@RestController
@RequestMapping("/resource")
@Api(value = "Resource", tags = "资源")
public class ResourceController {

    @Autowired
    private ResourceService resourceService;

    /**
     * 查询用户可用的所有资源
     */
    @ApiOperation(value = "查询用户可用的所有资源")
    @GetMapping
    public ApiResponse<List<AuthResource>> visible(String userId) {
        return ApiResponse.success(resourceService.queryPermissions(userId));
    }

    /**
     * 查询所有资源
     */
    @GetMapping("/list")
    @ApiOperation(value = "查询所有资源")
    public ApiResponse<List<String>> list() {
        return ApiResponse.success(resourceService.list());
    }
}
