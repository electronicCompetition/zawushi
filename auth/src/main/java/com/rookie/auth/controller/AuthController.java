package com.rookie.auth.controller;

import com.rookie.auth.common.base.ApiResponse;
import com.rookie.auth.dto.LoginParamDto;
import com.rookie.auth.service.AuthService;
import com.rookie.auth.service.ValidateCodeService;
import com.rookie.auth.vo.LoginVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Api(tags = "认证控制器")
@Validated
@Controller
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private ValidateCodeService validateCodeService;

    @Autowired
    private AuthService authService;

    @GetMapping(value = "captcha", produces = "image/png")
    @ApiOperation(value = "验证码")
    @ApiImplicitParam(name = "key", value = "验证码key", required = true)
    public void create(String key, HttpServletResponse response) throws IOException {
        validateCodeService.create(key, response);
    }

    @PostMapping("check")
    @ApiOperation(value = "校验验证码")
    public ApiResponse<Boolean> check(@RequestBody @Validated LoginParamDto loginParamDto) {
        return ApiResponse.success(validateCodeService.check(loginParamDto.getKey(), loginParamDto.getCode()));
    }

    @ApiOperation(value = "登录")
    @RequestMapping(value = "login", method = RequestMethod.POST)
    @ResponseBody
    public ApiResponse<LoginVo> login(@RequestBody @Validated LoginParamDto loginParamDto) {
        // 校验验证码是否正确
        if (validateCodeService.check(loginParamDto.getKey(), loginParamDto.getCode())) {
            return authService.login(loginParamDto.getAccount(), loginParamDto.getPassword());
        }
        return ApiResponse.success(null);
    }

    @ApiOperation(value = "注销")
    @RequestMapping(value = "logout", method = RequestMethod.GET)
    @ResponseBody
    public ApiResponse<Boolean> logout() {
        return ApiResponse.success();
    }
}
