package com.rookie.auth.controller;

import com.rookie.auth.common.base.ApiResponse;
import com.rookie.auth.service.MenuService;
import com.rookie.auth.vo.MenuTreeVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Slf4j
@Validated
@RestController
@RequestMapping("menu")
@Api(tags = "菜单")
public class MenuController {

    @Autowired
    private MenuService menuService;

    @ApiOperation(value = "查询用户可用的所有菜单")
    @ApiImplicitParam(name = "userId", value = "用户id", dataType = "String", paramType = "query")
    @GetMapping
    public ApiResponse<List<MenuTreeVo>> queryUserMenu(@NotEmpty String userId) {
        return ApiResponse.success(menuService.queryUserMenu(userId));
    }

}