package com.rookie.auth.controller;

import com.rookie.auth.common.FastDFSClient;
import com.rookie.auth.common.FastDFSFileMeta;
import com.rookie.auth.common.base.ApiResponse;
import com.rookie.auth.entity.Attachment;
import com.rookie.auth.service.AttachmentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.FilenameUtils;
import org.csource.common.MyException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Controller
@RequestMapping("/attachment")
@Api(tags = "通用附件上传下载")
public class AttachmentController {

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    private FastDFSClient fastDFSClient;

    @ApiOperation(value = "文件上传")
    @ApiImplicitParam(name = "file", value = "上传文件", required = true, dataType = "MultipartFile", allowMultiple = true)
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    @ResponseBody
    public ApiResponse<Map<String, String>> upload(@RequestParam("file") MultipartFile file) throws IOException {
        String fileName = file.getOriginalFilename();   // 获取文件名
        String ext = fileName.substring(fileName.lastIndexOf(".") + 1); // 获取文件类型

        InputStream in = file.getInputStream();
        int size = 0;
        while (size == 0) {
            size = in.available();
        }
        byte[] file_buff = new byte[size];
        in.read(file_buff);
        in.close();

        String groupName = "";
        String remoteFileName = "";
        FastDFSFileMeta fastDFSFile = new FastDFSFileMeta(fileName, file_buff, ext);
        try {
            String[] uploadResults = fastDFSClient.upload(fastDFSFile);
            if (uploadResults != null && uploadResults.length > 1) {
                groupName = uploadResults[0];
                remoteFileName = uploadResults[1];
            }
        } catch (Exception e) {
        }

        // 将附件保存到数据库
        Attachment attachment = new Attachment();
        attachment.setId(UUID.randomUUID().toString().replace("-", ""));
        attachment.setFileName(fileName);
        attachment.setSize(size);
        attachment.setSuffix(FilenameUtils.getExtension(fileName));
        attachment.setGroupName(groupName);
        attachment.setRemoteFileName(remoteFileName);
        attachment.setUrl(null);
        attachment.setDelFlag("0");
        attachment.setCreateUserId("yzliu15");
        attachment.setCreateTime(new Date());
        attachment.setLastUpdateUserId("yzliu15");
        attachment.setLastUpdateTime(new Date());

        // 保存附件信息
        attachmentService.saveAttachment(attachment);

        // 调用成功
        Map<String, String> result = new HashMap<>();
        result.put("fileId", attachment.getId());
        result.put("fileName", fileName);
        result.put("fileSize", String.valueOf(size));
        result.put("suffix", FilenameUtils.getExtension(fileName));
        return ApiResponse.success(result);
    }

    @ApiOperation(value = "文件下载")
    @ApiImplicitParam(name = "fileId", value = "文件Id", required = true, dataType = "String")
    @RequestMapping(value = "/download", method = RequestMethod.POST)
    @ResponseBody
    public void download(String fileId, HttpServletResponse response) throws Exception {
        // 获取附件信息
        Attachment attachment = attachmentService.queryAttachmentById(fileId);
        if (attachment != null) {
            String groupName = attachment.getGroupName();
            String remoteFileName = attachment.getRemoteFileName();

            // 获取文件流
            InputStream in = fastDFSClient.download(groupName, remoteFileName);

            response.setCharacterEncoding("utf-8");
            response.setContentType("multipart/form-data");
            response.setHeader("Content-Disposition",
                    "attachment; filename=\"" + URLEncoder.encode(attachment.getFileName(), "utf-8") + "\"");

            // 写出文件
            OutputStream os = response.getOutputStream();
            byte[] b = new byte[2048];
            int length;
            while ((length = in.read(b)) > 0) {
                os.write(b, 0, length);
            }

            // 这里主要关闭。
            os.close();
            in.close();
        } else {
            response.setContentType("text/html;charset=utf-8");
            response.getWriter().write("文件不存在");
        }
    }

    @ApiOperation(value = "文件删除")
    @ApiImplicitParam(name = "fileId", value = "文件Id", required = true, dataType = "String")
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public ApiResponse<Boolean> download(String fileId) throws IOException, MyException {
        // 获取附件信息
        Attachment attachment = attachmentService.queryAttachmentById(fileId);
        if (attachment != null) {
            String groupName = attachment.getGroupName();
            String remoteFileName = attachment.getRemoteFileName();
            fastDFSClient.delete(groupName, remoteFileName);
        }

        return ApiResponse.success();
    }
}
