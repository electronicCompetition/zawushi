package com.rookie.auth.controller;

import com.rookie.auth.common.base.ApiResponse;
import com.rookie.auth.entity.RegionTree;
import com.rookie.auth.service.DictService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/dict")
@Api(tags = "通用字典")
public class DictController {

    @Autowired
    private DictService dictService;

    @ApiOperation(value = "字典类型查询")
    @RequestMapping(value = "queryDictItem", method = RequestMethod.POST)
    @ResponseBody
    public ApiResponse<List<Map<String, String>>> queryDictItem() {
        return ApiResponse.success(dictService.queryDictItem());
    }

    @ApiOperation(value = "字典查询")
    @ApiImplicitParam(name = "dictType", value = "字典类型", required = true, dataType = "String")
    @RequestMapping(value = "queryDict", method = RequestMethod.POST)
    @ResponseBody
    public ApiResponse<List<Map<String, String>>> queryDict(String dictType) {
        return ApiResponse.success(dictService.queryDict(dictType));
    }

    @ApiOperation(value = "省市区查询")
    @ApiImplicitParam(name = "regionCode", value = "区域code", dataType = "String")
    @RequestMapping(value = "queryRegion", method = RequestMethod.POST)
    @ResponseBody
    public ApiResponse<List<Map<String, String>>> queryRegion(String regionCode) {
        return ApiResponse.success(dictService.queryRegion(regionCode));
    }

    @ApiOperation(value = "省市区树查询")
    @RequestMapping(value = "queryRegionTree", method = RequestMethod.GET)
    @ResponseBody
    public ApiResponse<List<RegionTree>> queryRegionTree() {
        return ApiResponse.success(dictService.queryRegionTree());
    }
}
