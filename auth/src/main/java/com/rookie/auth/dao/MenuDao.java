package com.rookie.auth.dao;

import com.rookie.auth.vo.MenuTreeVo;

import java.util.List;

public interface MenuDao {

    /**
     * 查询用户可用菜单
     * @param userId 指定用户id
     * @return
     */
    List<MenuTreeVo> queryUserMenu(String userId);
}
