package com.rookie.auth.dao;

import com.rookie.auth.entity.RegionTree;

import java.util.List;
import java.util.Map;

public interface DictDao {

    /**
     * 缓存字典类型查询字典列表
     * @return List<Map<String, String>>
     */
    List<Map<String, String>> queryCacheDict();

    /**
     * 查询字典项列表
     * @return List<Map<String, String>>
     */
    List<Map<String, String>> queryDictItem();

    /**
     * 根据字典类型查询字典列表
     * @param dictType 字典类型
     * @return List<Map<String, String>>
     */
    List<Map<String, String>> queryDict(String dictType);

    /**
     * 省市区查询
     * @param regionCode 上级区域code
     * @return List<Map<String, String>>
     */
    List<Map<String, String>> queryRegion(String regionCode);

    /**
     * 查询全部行政区域信息
     * @return List<RegionTree>
     */
    List<RegionTree> queryRegionAll();
}
