package com.rookie.auth.dao;

import com.rookie.auth.entity.AuthResource;

import java.util.List;

public interface ResourceDao {

    /**
     * 查询账号可访问的资源信息
     * @param userId 用户id
     * @return List<AuthResource>
     */
    List<AuthResource> queryPermissions(String userId);

    /**
     * 查询所有资源
     * @return
     */
    List<AuthResource> queryAll();
}
