package com.rookie.auth.dao;

import com.rookie.auth.entity.AuthUser;

public interface AuthDao {

    /**
     * 根据账号查询用户信息
     * @param account 账号
     * @return AuthUser
     */
    AuthUser queryByAccount(String account);
}
