package com.rookie.auth.dao;


import com.rookie.auth.entity.Attachment;

public interface AttachmentDao {

    int deleteByPrimaryKey(String id);

    int insert(Attachment record);

    Attachment selectByPrimaryKey(String id);
}
