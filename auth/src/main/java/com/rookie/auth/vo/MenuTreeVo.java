package com.rookie.auth.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;


/**
 * 树形菜单 DTO
 *
 */
@Data
@ApiModel(description = "菜单树")
public class MenuTreeVo {

    @ApiModelProperty(value = "id")
    private String id;

    @ApiModelProperty(value = "菜单名称")
    private String name;

    @ApiModelProperty(value = "菜单路由path")
    private String path;

    @ApiModelProperty(value = "菜单路由组件component")
    private String component;

    @ApiModelProperty(value = "icon")
    private String icon;

    @ApiModelProperty(value = "父级id")
    private String parentId;

    @ApiModelProperty(value = "子类菜单")
    private List<MenuTreeVo> children;

}
