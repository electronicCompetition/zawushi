package com.rookie.auth.vo;

import com.rookie.auth.entity.AuthUser;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@ApiModel(value = "LoginVo", description = "登录信息")
public class LoginVo implements Serializable {

    private static final long serialVersionUID = -3124612657759050173L;

    @ApiModelProperty(value = "用户信息")
    private AuthUser user;

    @ApiModelProperty(value = "token")
    private String token;

    @ApiModelProperty(value = "权限列表")
    private List<String> permissionList;
}
