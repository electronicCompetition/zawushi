package com.css.service.impl;

import com.css.service.DemoService;
import org.springframework.aop.framework.AopContext;
import org.springframework.stereotype.Service;

/**
 * 同一个类下，调用方法，采用AopContext方式，解决Spring注解失效
 */
@Service
public class DemoServiceImpl implements DemoService {


    @Override
    public void methodA() {
        // A方法内调用B方法，使用代理调用，经过切面可以使事务生效
        ((DemoServiceImpl) AopContext.currentProxy()).methodB();
    }

    @Override
    public void methodB() {
        System.out.println("methodB");
    }
}
