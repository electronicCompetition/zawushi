package com.rookie.fastjson;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;

import java.util.List;
import java.util.Map;

public class TestJson {
    public static void main(String[] args) {
        // 将json文本解析为java对象
        String jsonStr = "{\"password\":\"123456\",\"username\":\"demo\"}";
        User user = JSONObject.parseObject(jsonStr, User.class);
        System.out.println(user);

        // 将json文本解析为List集合
        jsonStr = "[{\"password\":\"123123\",\"username\":\"tom\"},{\"password\":\"321321\",\"username\":\"cat\"}]";
        List<User> users = JSONObject.parseArray(jsonStr, User.class);
        users.forEach(System.out::println);

        // 将java对象转为json文本
        user = new User("123456", "yzliu15", "1qaz@WSX3edc");
        String s = JSONObject.toJSONString(user);
        System.out.println(s);

        // json字符串转复杂java对象
        jsonStr = "{\"name\":\"userGroup\",\"users\":[{\"password\":\"123123\",\"username\":\"zhangsan\"},{\"password\":\"321321\",\"username\":\"lisi\"}]}";
        // JSONObject 相当于map
        JSONObject jsonObject = (JSONObject) JSONObject.parse(jsonStr);
        String name = jsonObject.getString("name");
        System.out.println(name);

        String userStr = jsonObject.getString("users");
        users = JSONObject.parseArray(userStr, User.class);
        users.forEach(System.out::println);

        // 将json文本解析为List集合
        jsonStr = "[{\"password\":\"123123\",\"username\":\"tom\"},{\"password\":\"321321\",\"username\":\"cat\"}]";
        List<Map<String, String>> maps = JSONObject.parseObject(jsonStr, new TypeReference<List<Map<String, String>>>() {});
        maps.forEach(System.out::println);
    }
}
