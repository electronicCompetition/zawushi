package com.css;


import com.css.vo.GoodsVO;
import com.css.vo.UserVo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class DictSerializerAppTest {
    @Test
    public void test() throws JsonProcessingException {
        UserVo user = new UserVo();
        user.setSex("0");
        System.out.println(new ObjectMapper().writeValueAsString(user));
    }

    @Test
    public void test2() throws JsonProcessingException {
        GoodsVO goodsVO = new GoodsVO();
        goodsVO.setPrice(38.23456789D);
        System.out.println(new ObjectMapper().writeValueAsString(goodsVO));
    }
}
