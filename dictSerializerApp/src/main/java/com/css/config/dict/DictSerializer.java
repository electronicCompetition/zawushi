package com.css.config.dict;

import com.css.dao.DictMapper;
import com.css.entity.Dict;
import com.css.entity.DictExample;
import com.css.utils.SpringContextUtil;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class DictSerializer extends StdSerializer<Object> {

    private static final DictMapper dictMapper = (DictMapper) SpringContextUtil.getBean("dictMapper");

    private static final StringRedisTemplate stringRedisTemplate = (StringRedisTemplate) SpringContextUtil.getBean("stringRedisTemplate");


    // 模拟DB
//    private static final Map<String, String> map = new HashMap<>();
//
//    public static LoadingCache<String, String> createGuavaCache() {
//        return CacheBuilder.newBuilder()
//                // 设置并发级别为5，并发级别是指可以同时写缓存的线程数
//                .concurrencyLevel(5)
//                // 设置写缓存后5分钟后过期
//                .expireAfterWrite(5, TimeUnit.MINUTES)
//                // 设置缓存容器的初始容量为8
//                .initialCapacity(8)
//                // 设置缓存最大容量为10，超过10之后就会按照LRU最近虽少使用算法来移除缓存项
//                .maximumSize(10)
//                // 设置统计缓存的各种统计信息（生产坏境关闭）
//                .recordStats()
//                // 设置缓存的移除通知
//                .removalListener(new RemovalListener<Object, Object>() {
//                    @Override
//                    public void onRemoval(RemovalNotification<Object, Object> notification) {
//                        System.out.println(notification.getKey() + " was removed, cause is " + notification.getCause());
//                    }
//                })
//                // 指定CacheLoader，在缓存不存在时通过CacheLoader的实现自动加载缓存
//                .build(new CacheLoader<String, String>() {
//                    @Override
//                    public String load(String key) throws Exception {
//                        // 往DB中查询数据
//                        System.out.println("查询key:" + key + "的数据");
//                        return map.get(key);
//                    }
//                });
//    }

    public DictSerializer() {
        super(Object.class);
    }

    /**
     * 执行对象的序列化
     * Object value：待序列化的值，不能为null哦
     * JsonGenerator gen：生成器
     * SerializerProvider serializers：可用于获取序列化器的提供程序
     */
    @Override
    public void serialize(Object code, JsonGenerator gen, SerializerProvider provider) throws IOException {
        // 缓存中获取字典项
        String label = stringRedisTemplate.opsForValue().get(code.toString());
        if (StringUtils.hasText(label)) {
            gen.writeStartObject();
            gen.writeStringField("code", code.toString());
            gen.writeStringField("label", label);
            gen.writeEndObject();
            return;
        }

        DictExample dictExample = new DictExample();
        dictExample.createCriteria().andDictCodeEqualTo(code.toString());
        List<Dict> dictList = dictMapper.selectByExample(dictExample);
        if (!CollectionUtils.isEmpty(dictList) && StringUtils.hasText(dictList.get(0).getDictName())) {
            stringRedisTemplate.opsForValue().set(code.toString(), dictList.get(0).getDictName(), 5, TimeUnit.MINUTES);

            gen.writeStartObject();
            gen.writeStringField("code", code.toString());
            gen.writeStringField("label", dictList.get(0).getDictName());
            gen.writeEndObject();
            return;
        }

        gen.writeNull();
    }
}
