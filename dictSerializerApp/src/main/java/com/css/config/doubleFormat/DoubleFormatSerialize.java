package com.css.config.doubleFormat;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Objects;

public class DoubleFormatSerialize extends StdSerializer<Double> implements ContextualSerializer {

    /**
     * 小数点后保留位数
     */
    private int bitNum;

    protected DoubleFormatSerialize() {
        super(Double.class);
    }

    @Override
    public JsonSerializer<?> createContextual(SerializerProvider prov, BeanProperty property) throws JsonMappingException {
        if (property != null) {
            // 非 Double 直接跳过
            if (Objects.equals(property.getType().getRawClass(), Double.class)) {
                // 获取注解信息
                DoubleFormat annotation = property.getAnnotation(DoubleFormat.class);
                if (annotation == null) {
                    annotation = property.getContextAnnotation(DoubleFormat.class);
                }
                if (annotation != null) {
                    // 获得注解上的值并赋值
                    this.bitNum = annotation.value();
                    return this;
                }
            }
            return prov.findValueSerializer(property.getType(), property);
        }
        return prov.findNullValueSerializer(null);
    }

    @Override
    public void serialize(Double value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        StringBuilder pattern = new StringBuilder("#0.");
        for (int i = 0; i < this.bitNum; i++) {
            // 追加位数
            pattern.append("0");
        }
        // 返回值
        gen.writeString(new DecimalFormat(pattern.toString()).format(value));
    }
}
