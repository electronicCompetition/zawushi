package com.css.vo;

import com.css.config.doubleFormat.DoubleFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GoodsVO {

    private Long id;
    private String goodsName;
    /**商品名*/
    @DoubleFormat(value = 3)
    private Double price;
}
