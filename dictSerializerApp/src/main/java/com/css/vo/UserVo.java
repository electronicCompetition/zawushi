package com.css.vo;

import com.css.config.dict.Dict;
import lombok.Data;

@Data
public class UserVo {

    private String username;

    @Dict
    private String sex;
}
