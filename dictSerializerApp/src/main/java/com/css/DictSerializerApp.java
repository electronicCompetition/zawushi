package com.css;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.css.dao")
@SpringBootApplication
public class DictSerializerApp {

    public static void main(String[] args) {
        SpringApplication.run(DictSerializerApp.class, args);
    }
}
