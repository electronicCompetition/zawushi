package com.css.utils;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class SpringContextUtil implements ApplicationContextAware {

    private static ApplicationContext context;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }

    public static Object getBean(String beanName) {
        try {
            return context.getBean(beanName);
        } catch (Exception e) {
            return null;
        }
    }

    public static Object getBean(Class<?> arg0) {
        return context.getBean(arg0);
    }

    public static AutowireCapableBeanFactory getCapableBeanFactory() {
        return context.getAutowireCapableBeanFactory();
    }

    public static Object getBeanByInterface(Class<?> clazz) {
        Map<String, ?> beans = context.getBeansOfType(clazz);
        Object obj = null;
        for (Map.Entry<String, ?> entry : beans.entrySet()) {
            String key = entry.getKey();
            obj = getBean(key);
            Primary ann = obj.getClass().getAnnotation(Primary.class);
            if (ann != null) {
                break;
            }
        }
        return obj;
    }

    public static <T> Map<String, T> getBeans(Class<T> clazz) {
        Map<String, T> beansOfType = context.getBeansOfType(clazz);

        return beansOfType;
    }
}
