package com.css;

import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class Demo {

    /**
     * dosomething
     *
     * @param item
     * @return
     */
    private Object doSomething(String item) {
        System.out.println("doSomething:\t" + item);
        return item;
    }

    /**
     * 第一种处理方式
     */
    @Test
    public void LambdaTryTest1() {
        List<String> myList = Arrays.asList("1", "2", "3", "4", "5", "6");

        myList.stream().map(item -> {
            try {
                return doSomething(item);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }).forEach(System.out::println);
    }


    /**
     * 第二种处理方式
     */
    @Test
    public void LambdaTryTest2() {
        List<String> myList = Arrays.asList("1", "2", "3", "4", "5", "6");
        myList.stream().map(this::doSomething).forEach(System.out::println);
    }

    private Object trySomething(String item) {
        try {
            return doSomething(item);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    /**
     * 第三种处理方式
     */
    @Test
    public void LambdaTryTest3() {
        List<String> myList = Arrays.asList("1", "2", "3", "4", "5", "6");
        myList.stream()
                .map(wrap(this::doSomething))
                .forEach(System.out::println);
    }

    //定义一个检查接口
    @FunctionalInterface
    public interface CheckedFunction<T, R> {
        R apply(T t) throws Exception;
    }

    public static <T, R> Function<T, R> wrap(CheckedFunction<T, R> checkedFunction) {
        return t -> {
            try {
                return checkedFunction.apply(t);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        };
    }
}
