package com.rookie.springbootpomprofilesactive.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class HelloController {

    @Value("${server-addr}")
    private String serverAddr;

    @Value("${namespace}")
    private String namespace;

    @RequestMapping("hello")
    public Map say() {
        return new HashMap<String, String>() {{
            put("serverAddr", serverAddr);
            put("namespace", namespace);
        }};
    }
}
