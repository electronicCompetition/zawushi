package com.rookie.springbootpomprofilesactive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootPomProfilesActiveApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootPomProfilesActiveApplication.class, args);
    }

}
