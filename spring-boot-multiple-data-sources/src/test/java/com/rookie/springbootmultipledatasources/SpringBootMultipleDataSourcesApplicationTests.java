package com.rookie.springbootmultipledatasources;

import com.rookie.springbootmultipledatasources.config.DataSourceContextHolder;
import com.rookie.springbootmultipledatasources.config.DataSourceType;
import com.rookie.springbootmultipledatasources.dao.Mapper;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest(classes={SpringBootMultipleDataSourcesApplication.class})
class SpringBootMultipleDataSourcesApplicationTests {

    @Autowired
    private Mapper mapper;

    @Test
    void contextLoads() {
        List<Map<String, String>> master = mapper.findMaster();
        master.forEach(System.out::println);

        System.out.println("---------------------------------------------------");

        DataSourceContextHolder.setDataSource(DataSourceType.DB_SLAVE);
        List<Map<String, String>> slave = mapper.findSlave();
        DataSourceContextHolder.clearDataSource();
        slave.forEach(System.out::println);
    }

}
