package com.rookie.springbootmultipledatasources.config;

import lombok.extern.slf4j.Slf4j;

/**
 * 动态数据源上下文管理：设置数据源，获取数据源，清除数据源
 */
@Slf4j
public class DataSourceContextHolder {

    // 存放当前线程使用的数据源类型
    private static final ThreadLocal<DataSourceType> contextHolder = new ThreadLocal<>();

    // 设置数据源
    public static void setDataSource(DataSourceType type) {
        log.info("数据源切换为：{}", type);
        contextHolder.set(type);
    }

    // 获取数据源
    public static DataSourceType getDataSource() {
        DataSourceType lookUpKey = contextHolder.get();
        return lookUpKey == null ? DataSourceType.DB_MASTER : lookUpKey;
    }

    // 清除数据源
    public static void clearDataSource() {
        contextHolder.remove();
    }
}