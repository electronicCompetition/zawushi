package com.rookie.springbootmultipledatasources.config;

public enum DataSourceType {
    DB_MASTER,
    DB_SLAVE
}
