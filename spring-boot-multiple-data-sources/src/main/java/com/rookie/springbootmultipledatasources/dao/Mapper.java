package com.rookie.springbootmultipledatasources.dao;

import java.util.List;
import java.util.Map;

public interface Mapper {

    List<Map<String, String>> findMaster();

    List<Map<String, String>> findSlave();


}
