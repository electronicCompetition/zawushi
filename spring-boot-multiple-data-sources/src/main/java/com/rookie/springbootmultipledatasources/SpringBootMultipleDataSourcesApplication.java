package com.rookie.springbootmultipledatasources;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@MapperScan(basePackages = {"com.rookie.springbootmultipledatasources.dao"})
public class SpringBootMultipleDataSourcesApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootMultipleDataSourcesApplication.class, args);
    }

}
