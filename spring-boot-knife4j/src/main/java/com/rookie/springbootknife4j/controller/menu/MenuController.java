package com.rookie.springbootknife4j.controller.menu;

import com.rookie.springbootknife4j.entity.Menu;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("menu")
@Api(tags = "菜单控制器")
public class MenuController {

    @GetMapping("getUsers")
    @ApiOperation(value = "查询所有菜单信息", notes = "查询所有菜单信息")
    public List<Menu> getUsers(){
        List<Menu> list = new ArrayList<>();
        Menu menu = new Menu();
        menu.setId(1);
        menu.setName("日志管理菜单");
        list.add(menu);
        menu.setId(2);
        menu.setName("权限管理菜单");
        list.add(menu);
        return list;
    }

    @GetMapping("save")
    @ApiOperation(value = "新增菜单信息", notes = "新增菜单信息")
    public String save(@RequestBody Menu menu) {
        return "success";
    }

    @GetMapping("delete")
    @ApiOperation(value = "删除菜单信息", notes = "删除菜单信息")
    @ApiImplicitParam(name = "id", value = "菜单id", required = true, dataType = "Integer")
    public String delete(Integer id) {
        return "success";
    }

    @GetMapping("findByPage")
    @ApiOperation(value = "分页查询菜单信息", notes = "分页查询菜单信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页码", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "pageSize", value = "每页条数", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "search", value = "查询条件", dataType = "String")

    })
    public String findByPage(Integer pageNum, Integer pageSize, String search) {
        return "success";
    }

}
