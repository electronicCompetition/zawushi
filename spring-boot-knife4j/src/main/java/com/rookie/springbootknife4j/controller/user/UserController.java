package com.rookie.springbootknife4j.controller.user;

import com.rookie.springbootknife4j.entity.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("user")
@Api(tags = "用户控制器")
public class UserController {

    @GetMapping("getUsers")
    @ApiOperation(value = "查询所有用户信息", notes = "查询所有用户信息")
    public List<User> getUsers(){
        List<User> list = new ArrayList<>();
        User user = new User();
        user.setId(1);
        user.setName("yzliu15");
        user.setAge(20);
        user.setAddress("wuhu");
        list.add(user);
        return list;
    }

    @GetMapping("save")
    @ApiOperation(value = "新增用户信息", notes = "新增用户信息")
    public String save(@RequestBody User user) {
        return "success";
    }

    @GetMapping("delete")
    @ApiOperation(value = "删除用户信息", notes = "删除用户信息")
    @ApiImplicitParam(name = "id", value = "用户id", required = true, dataType = "Integer")
    public String delete(Integer id) {
        return "success";
    }

    @GetMapping("findByPage")
    @ApiOperation(value = "分页查询用户信息", notes = "分页查询用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页码", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "pageSize", value = "每页条数", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "search", value = "查询条件", dataType = "String")

    })
    public String findByPage(Integer pageNum, Integer pageSize, String search) {
        return "success";
    }

}
