package com.rookie.springbootglobalexception.controller;

import com.rookie.springbootglobalexception.base.R;
import com.rookie.springbootglobalexception.exception.ExceptionCode;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/rookie")
public class RookieController {

    @GetMapping("success")
    public R<Map<String, Object>> success() {
        return R.success(new HashMap<String, Object>() {{ put("name", "zhangsan"); put("age", 24); }});
    }

    @GetMapping("/fail")
    public R<List<String>> fail() {
        return R.fail(ExceptionCode.SYSTEM_BUSY);
    }
}
