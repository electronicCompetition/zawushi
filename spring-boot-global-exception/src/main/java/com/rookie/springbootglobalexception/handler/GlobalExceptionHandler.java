package com.rookie.springbootglobalexception.handler;

import com.rookie.springbootglobalexception.base.R;
import com.rookie.springbootglobalexception.exception.BusinessException;
import com.rookie.springbootglobalexception.exception.ExceptionCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    /**
     * 业务异常
     *
     * @param ex
     * @param request
     * @return
     */
    @ExceptionHandler(BusinessException.class)
    public R<String> bizException(BusinessException ex, HttpServletRequest request) {
        log.warn("BusinessException:", ex);
        return R.result(ex.getCode(), "", ex.getMessage()).setPath(request.getRequestURI());
    }

    /**
     * 空指针异常
     *
     * @param ex
     * @param request
     * @return
     */
    @ExceptionHandler(NullPointerException.class)
    public R<String> nullPointerException(NullPointerException ex, HttpServletRequest request) {
        log.warn("NullPointerException:", ex);
        return R.result(ExceptionCode.NULL_POINT_EX.getCode(), "", ExceptionCode.NULL_POINT_EX.getMsg()).setPath(request.getRequestURI());
    }

    /**
     * 无效参数异常
     *
     * @param ex
     * @param request
     * @return
     */
    @ExceptionHandler(IllegalArgumentException.class)
    public R<String> illegalArgumentException(IllegalArgumentException ex, HttpServletRequest request) {
        log.warn("IllegalArgumentException:", ex);
        return R.result(ExceptionCode.ILLEGAL_ARGUMENT_EX.getCode(), "", ExceptionCode.ILLEGAL_ARGUMENT_EX.getMsg()).setPath(request.getRequestURI());
    }

    /**
     * hibernate-validator 验证异常
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public R<String> methodArgumentNotValidException(MethodArgumentNotValidException ex, HttpServletRequest request) {
        log.warn("MethodArgumentNotValidException:", ex);
        return R.result(ExceptionCode.BASE_VALID_PARAM.getCode(), "", ex.getBindingResult().getFieldError().getDefaultMessage()).setPath(request.getRequestURI());
    }

    /**
     * 运行SQL出现异常
     *
     * @param ex
     * @param request
     * @return
     */
    @ExceptionHandler(SQLException.class)
    public R<String> sqlException(SQLException ex, HttpServletRequest request) {
        log.warn("SQLException:", ex);
        return R.result(ExceptionCode.SQL_EX.getCode(), "", ExceptionCode.SQL_EX.getMsg()).setPath(request.getRequestURI());
    }

    /**
     * 其他异常
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(Exception.class)
    public R<String> otherExceptionHandler(Exception ex, HttpServletRequest request) {
        log.warn("Exception:", ex);
        if (ex.getCause() instanceof BusinessException) {
            return this.bizException((BusinessException) ex.getCause(), request);
        }
        return R.result(ExceptionCode.SYSTEM_BUSY.getCode(), "", ExceptionCode.SYSTEM_BUSY.getMsg()).setPath(request.getRequestURI());
    }
}
