package com.rookie.springbootredis;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rookie.springbootredis.entity.User;
import com.rookie.springbootredis.util.RedisUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.Map;

@SpringBootTest
class SpringBootRedisApplicationTests {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    private ObjectMapper mapper = new ObjectMapper();

    @Test
    void testStringValue() {
        redisTemplate.opsForValue().set("name", "斗鱼");
        System.out.println(redisTemplate.opsForValue().get("name"));
    }

    @Test
    void testSaveUser() {
        User user = new User("斗鱼", 18);
        redisTemplate.opsForValue().set("user", user);
        System.out.println(redisTemplate.opsForValue().get("user"));
    }

    //--------------------------------------------------------------

    @Test
    void testSaveUser2() throws JsonProcessingException {
        String user = mapper.writeValueAsString(new User("斗鱼", 18));
        stringRedisTemplate.opsForValue().set("user", user);
        System.out.println(stringRedisTemplate.opsForValue().get("user"));
    }

    @Test
    void testHash() {
        stringRedisTemplate.opsForHash().put("dict", "code", "001");
        stringRedisTemplate.opsForHash().put("dict", "name", "深度操作系统");

        Map<Object, Object> dict = stringRedisTemplate.opsForHash().entries("dict");
        System.out.println(dict.toString());
    }
}
