package workqueues;

import com.rabbitmq.client.*;
import util.RabbitMQUtils;

import java.io.IOException;

public class Customer1 {

    public static void main(String[] args) throws IOException {
        Connection connection = RabbitMQUtils.getConnection();
        assert connection != null;
        final Channel channel = connection.createChannel();
        channel.basicQos(1);    // 每次通道传递1个消息
        channel.queueDeclare("work", false, false, false, null);
        // 参数2：取消自动确认消息
        channel.basicConsume("work", false, new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("消费者-1：" + new String(body));
                // 手动确认
                // 参数1：确认队列中那个具体消息
                // 参数2：是否开启多个消息同时确认
                channel.basicAck(envelope.getDeliveryTag(), false);
            }
        });
    }
}
