package workqueues;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import org.junit.Test;
import util.RabbitMQUtils;

import java.io.IOException;

public class Provider {

    @Test
    public void sendMessage() throws IOException {
        Connection connection = RabbitMQUtils.getConnection();
        assert connection != null;
        Channel channel = connection.createChannel();
        channel.queueDeclare("work", false, false, false, null);
        for (int i = 0; i <= 20; i++) {
            channel.basicPublish("", "work", null, (i +"work queues").getBytes());
        }
        RabbitMQUtils.closeChannelAndConnection(channel, connection);
    }
}
