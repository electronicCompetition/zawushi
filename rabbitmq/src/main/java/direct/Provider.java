package direct;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import org.junit.Test;
import util.RabbitMQUtils;

import java.io.IOException;

public class Provider {

    @Test
    public void sendMessage() throws IOException {
        Connection connection = RabbitMQUtils.getConnection();
        assert connection != null;
        Channel channel = connection.createChannel();
        // 声明交换机
        channel.exchangeDeclare("logs_direct", "direct");
        // 发布消息
        String routingKey = "error";
        channel.basicPublish("logs_direct", routingKey, null,  ("这是direct模型发布的基于【" + routingKey + "】发送的消息").getBytes());
        RabbitMQUtils.closeChannelAndConnection(channel, connection);
    }
}
