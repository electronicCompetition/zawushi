package fanout;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import org.junit.Test;
import util.RabbitMQUtils;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Provider {

    @Test
    public void sendMessage() throws IOException, TimeoutException {
        // 获取连接对象
        Connection connection = RabbitMQUtils.getConnection();
        // 获取连接通道
        assert connection != null;
        Channel channel = connection.createChannel();
        // 声明交换机
        // 参数1：交换机名称
        // 参数2：交换机类型
        channel.exchangeDeclare("logs", "fanout");
        // 发布消息
        channel.basicPublish("logs", "", null, "fanout type message".getBytes());
        RabbitMQUtils.closeChannelAndConnection(channel, connection);
    }
}
