package topic;

import com.rabbitmq.client.*;
import util.RabbitMQUtils;

import java.io.IOException;

public class Customer1 {
    public static void main(String[] args) throws IOException {
        Connection connection = RabbitMQUtils.getConnection();
        assert connection != null;
        Channel channel = connection.createChannel();
        // 声明交换机
        channel.exchangeDeclare("logs_topic", "topic");
        // 创建临时队列
        String queue = channel.queueDeclare().getQueue();
        // 队列绑定交换机
        // 通配符 * 代表一个词
        // 通配符 # 代表不仅仅只有一个词
        channel.queueBind(queue, "logs_topic", "item.*");
        // 消费消息
        channel.basicConsume(queue, true, new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
                System.out.println("消费者-1：" + new String(body));
            }
        });
    }
}
