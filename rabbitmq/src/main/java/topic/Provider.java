package topic;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import org.junit.Test;
import util.RabbitMQUtils;

import java.io.IOException;

public class Provider {

    @Test
    public void sendMessage() throws IOException {
        Connection connection = RabbitMQUtils.getConnection();
        assert connection != null;
        Channel channel = connection.createChannel();
        // 声明交换机
        channel.exchangeDeclare("logs_topic", "topic");
        // 发布消息
        String routingKey = "item.save";
        channel.basicPublish("logs_topic", routingKey, null,  ("这是topic模型发布的基于【" + routingKey + "】发送的消息").getBytes());
        RabbitMQUtils.closeChannelAndConnection(channel, connection);
    }
}
