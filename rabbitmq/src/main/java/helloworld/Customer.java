package helloworld;

import com.rabbitmq.client.*;
import org.junit.Test;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Customer {

    public static void main(String[] args) throws IOException, TimeoutException {
        // 创建rabbitmq连接工厂
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("192.168.208.128");
        factory.setPort(5672);
        factory.setVirtualHost("/ems");
        factory.setUsername("ems");
        factory.setPassword("ems");
        // 获取连接对象
        Connection conn = factory.newConnection();
        // 创建通道
        Channel channel = conn.createChannel();
        // 绑定消息队列
        channel.queueDeclare("hello", false, false, false, null);
        // 消费消息
        channel.basicConsume("hello", true, new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println(new String(body));
            }
        });
//        channel.close();
//        conn.close();
    }

    @Test
    public void acceptMessage() throws IOException, TimeoutException {

    }
}
