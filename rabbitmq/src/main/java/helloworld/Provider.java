package helloworld;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.junit.Test;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Provider {

    @Test
    public void sendMessage() throws IOException, TimeoutException {
        // 创建连接mq的连接工厂对象
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("192.168.208.128");
        factory.setPort(5672);
        factory.setVirtualHost("/ems");
        factory.setUsername("ems");
        factory.setPassword("ems");
        // 获取连接对象
        Connection conn = factory.newConnection();
        // 获取连接通道
        Channel channel = conn.createChannel();
        // 通道绑定对应的消息队列
        // 参数1：绑定队列名称 不存在时自动创建
        // 参数2：队列持久化
        // 参数3：exclusive 是否独占队列
        // 参数4：autoDelete 是否消费完成之后自动删除队列
        // 参数4：额外附件参数
        channel.queueDeclare("hello", false, false, false, null);
        // 发布消息
        // 参数1：交换机名称
        // 参数2：队列名称
        // 参数3：传递消息的额外设置
        // 参数4：消息内容
        channel.basicPublish("", "hello", null, "hello rabbitmq".getBytes());
        channel.close();
        conn.close();
    }
}
