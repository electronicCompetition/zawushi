package util;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class RabbitMQUtils {

    private static ConnectionFactory factory;

    static {
        // 创建连接mq的连接工厂对象
        factory = new ConnectionFactory();
        factory.setHost("192.168.208.128");
        factory.setPort(5672);
        factory.setVirtualHost("/ems");
        factory.setUsername("ems");
        factory.setPassword("ems");
    }

    // 获取连接对象
    public static Connection getConnection(){
        try {
            return factory.newConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //关闭通道和连接
    public static void closeChannelAndConnection(Channel channel, Connection conn){
        try {
            if(channel != null) channel.close();
            if(conn != null)  conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
