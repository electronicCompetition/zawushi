package com.rookie.springbootfastdfs.service;


import com.rookie.springbootfastdfs.entity.Attachment;

public interface AttachmentService {

    void saveAttachment(Attachment attachment);

    Attachment queryAttachmentById(String id);
}
