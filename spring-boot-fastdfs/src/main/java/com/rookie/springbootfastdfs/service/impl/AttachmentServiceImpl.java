package com.rookie.springbootfastdfs.service.impl;

import com.rookie.springbootfastdfs.dao.AttachmentMapper;
import com.rookie.springbootfastdfs.entity.Attachment;
import com.rookie.springbootfastdfs.service.AttachmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AttachmentServiceImpl implements AttachmentService {

    @Autowired
    private AttachmentMapper attachmentMapper;

    @Override
    public void saveAttachment(Attachment attachment) {
        attachmentMapper.insert(attachment);
    }

    @Override
    public Attachment queryAttachmentById(String id) {
        return attachmentMapper.selectByPrimaryKey(id);
    }
}
