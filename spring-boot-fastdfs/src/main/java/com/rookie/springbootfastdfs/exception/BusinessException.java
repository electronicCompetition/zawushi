package com.rookie.springbootfastdfs.exception;


public class BusinessException extends RuntimeException {

    private static final long serialVersionUID = -778887391066124051L;

    /**
     * 具体异常码
     */
    private int code;

    public BusinessException(int code, String msg) {
        super(msg);
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

}
