package com.rookie.springbootfastdfs.entity;

import lombok.Data;

import java.util.Date;
@Data
public class Attachment {

    private String id;
    // 文件名
    private String fileName;
    // 文件大小
    private long size;
    // 文件前缀
    private String suffix;
    // 文件所在组
    private String groupName;
    // 远程文件名
    private String remoteFileName;
    // 远程文件地址
    private String url;
    private String delFlag;
    private String createUserId;
    private Date createTime;
    private String lastUpdateUserId;
    private Date lastUpdateTime;

}
