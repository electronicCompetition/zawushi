package com.rookie.springbootfastdfs.controller;

import com.linuxense.javadbf.DBFReader;
import com.rookie.springbootfastdfs.base.R;
import com.rookie.springbootfastdfs.entity.Attachment;
import com.rookie.springbootfastdfs.service.AttachmentService;
import com.rookie.springbootfastdfs.util.FastDFSClient;
import com.rookie.springbootfastdfs.util.FastDFSFileMeta;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

@RestController
@RequestMapping("/attachment")
@Api(tags = "通用附件上传下载")
public class AttachmentController {

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    private FastDFSClient fastDFSClient;

    @ApiOperation(value = "文件上传", notes = "文件上传")
    @ApiImplicitParam(name = "file", value = "上传文件", dataType = "MultipartFile", allowMultiple = true, paramType = "query")
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    @ResponseBody
    public R<Map<String, String>> upload(@RequestParam("file") MultipartFile[] file) throws IOException {
        String fileName = file[0].getOriginalFilename();   // 获取文件名
        String ext = fileName.substring(fileName.lastIndexOf(".") + 1); // 获取文件类型

        InputStream in = file[0].getInputStream();
        int size = 0;
        while (size == 0) {
            size = in.available();
        }
        byte[] file_buff = new byte[size];
        in.read(file_buff);
        in.close();

        String groupName = "";
        String remoteFileName = "";
        FastDFSFileMeta fastDFSFile = new FastDFSFileMeta(fileName, file_buff, ext);
        try {
            String[] uploadResults = fastDFSClient.upload(fastDFSFile);
            if (uploadResults != null && uploadResults.length > 1) {
                groupName = uploadResults[0];
                remoteFileName = uploadResults[1];
            }
        } catch (Exception e) {
        }

        // 将附件保存到数据库
        Attachment attachment = new Attachment();
        attachment.setId(UUID.randomUUID().toString().replace("-", ""));
        attachment.setFileName(fileName);
        attachment.setSize(size);
        attachment.setSuffix(FilenameUtils.getExtension(fileName));
        attachment.setGroupName(groupName);
        attachment.setRemoteFileName(remoteFileName);
        attachment.setUrl(null);
        attachment.setDelFlag("0");
        attachment.setCreateUserId("yzliu15");
        attachment.setCreateTime(new Date());
        attachment.setLastUpdateUserId("yzliu15");
        attachment.setLastUpdateTime(new Date());

        // 保存附件信息
        attachmentService.saveAttachment(attachment);

        // 调用成功
        Map<String, String> result = new HashMap<>();
        result.put("fileId", attachment.getId());
        result.put("fileName", fileName);
        result.put("fileSize", String.valueOf(size));
        result.put("suffix", FilenameUtils.getExtension(fileName));
        return R.success(result);
    }

    @ApiOperation(value = "文件下载", notes = "文件下载")
    @RequestMapping(value = "/download", method = RequestMethod.GET)
    @ResponseBody
    public void download(String fileId, HttpServletResponse response) throws Exception {
        // 获取附件信息
        Attachment attachment = attachmentService.queryAttachmentById(fileId);
        if (attachment != null) {
            String groupName = attachment.getGroupName();
            String remoteFileName = attachment.getRemoteFileName();

            // 获取文件流
            InputStream in = fastDFSClient.download(groupName, remoteFileName);

            response.setCharacterEncoding("utf-8");
            response.setContentType("multipart/form-data");
            response.setHeader("Content-Disposition",
                    "attachment; filename=\"" + URLEncoder.encode(attachment.getFileName(), "utf-8") + "\"");

            // 写出文件
            OutputStream os = response.getOutputStream();
            byte[] b = new byte[2048];
            int length;
            while ((length = in.read(b)) > 0) {
                os.write(b, 0, length);
            }

            // 这里主要关闭。
            os.close();
            in.close();
        } else {
            response.setContentType("text/html;charset=utf-8");
            response.getWriter().write("文件不存在");
        }
    }

    @ApiOperation(value = "上传文件", notes = "文件上传")
    @ApiImplicitParam(name = "file", value = "上传文件", dataType = "MultipartFile", allowMultiple = true, paramType = "query")
    @RequestMapping(value = "/uploadZip", method = RequestMethod.POST)
    public void uploadZip(@RequestParam("file") MultipartFile[] file) {
        uploadZip(file[0],
                Objects.requireNonNull(Thread.currentThread().getContextClassLoader().getResource("")).getPath() + "temp");
    }

    /**
     * 文件处理
     *
     * @param file
     * @param path
     */
    private synchronized void uploadZip(MultipartFile file, String path) {
        try {
            // 解析zip包
            decompressZip(file, path);

            // 对解压完成的内容进行校验
            List<String> allFile = getAllFile(path, false);
            allFile.forEach(System.out::println);

            // 对解压完成的内容进行上传以及数据库操作

        } finally {
            // 删除在解析完成后存放在服务器的临时文件
            deleteDirectory(path);
        }
    }

    /**
     * 解压zip文件
     * @param file
     */
    private void decompressZip(MultipartFile file, String targetPath) {
        try (ZipInputStream zin = new ZipInputStream(file.getInputStream(), Charset.forName("GBK"))) {
            // 实例化对象，指明要进行解压的文件
            ZipEntry entry = null;
            while ((entry = zin.getNextEntry()) != null) {
                if (entry.isDirectory()) {
                    File dir = new File(targetPath + File.separator + entry.getName());
                    if (!dir.exists()) {
                        dir.mkdirs();
                    }
                } else {
                    // 如果是文件，就先创建一个文件
                    File targetFile = new File(targetPath + File.separator + entry.getName());
                    // 保证这个文件的父文件夹必须要存在
                    if (!targetFile.getParentFile().exists()) {
                        targetFile.getParentFile().mkdirs();
                    }
                    targetFile.createNewFile();
                    // 将压缩文件内容写入到这个文件中
                    try (FileOutputStream fos = new FileOutputStream(targetFile)) {
                        int len;
                        byte[] buf = new byte[1024];
                        while ((len = zin.read(buf)) != -1) {
                            fos.write(buf, 0, len);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取路径下的所有文件/文件夹
     * @param directoryPath  需要遍历的文件夹路径
     * @param isAddDirectory 是否将子文件夹的路径也添加到list集合中
     * @return
     */
    public List<String> getAllFile(String directoryPath, boolean isAddDirectory) {
        List<String> list = new ArrayList<>();
        File baseFile = new File(directoryPath);
        if (baseFile.isFile() || !baseFile.exists()) {
            return list;
        }
        File[] files = baseFile.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                if (isAddDirectory) {
                    list.add(file.getAbsolutePath());
                }
                list.addAll(getAllFile(file.getAbsolutePath(), isAddDirectory));
            } else {
                list.add(file.getAbsolutePath());
            }
        }
        return list;
    }

    /**
     * 删除在解析完成后存放在服务器的临时文件
     * @param path
     */
    public void deleteDirectory(String path) {
        try {
            FileUtils.deleteDirectory(new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @ApiOperation(value = "上传dbf文件解析", notes = "上传dbf文件解析")
    @ApiImplicitParam(name = "file", value = "上传文件", dataType = "MultipartFile", allowMultiple = true, paramType = "query")
    @RequestMapping(value = "/uploadDBF", method = RequestMethod.POST)
    public void uploadDBF(@RequestParam("file") MultipartFile[] file) {
        try (InputStream in = file[0].getInputStream()) {
            DBFReader reader = new DBFReader(in, Charset.forName("GBK"));

            // 调用DBFReader对实例方法得到dbf文件中字段的个数
            int fieldsCount = reader.getFieldCount();
            List<String> fields = new ArrayList<>(fieldsCount);
            for (int i = 0; i < fieldsCount; i++) {
                fields.add(reader.getField(i).getName());
            }

            List<HashMap<String, Object>> list = new ArrayList<>();
            HashMap<String, Object> map = null;

            // 一条条取出dbf文件中数据
            Object[] rowValues = null;
            while ((rowValues = reader.nextRecord()) != null) {
                map = new HashMap<>();
                for (int i = 0; i < rowValues.length; i++) {
                    map.put(fields.get(i), rowValues[i]);
                }
                list.add(map);
            }

            list.forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
