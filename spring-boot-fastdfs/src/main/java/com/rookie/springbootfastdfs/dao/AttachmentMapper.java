package com.rookie.springbootfastdfs.dao;


import com.rookie.springbootfastdfs.entity.Attachment;

public interface AttachmentMapper {

    int deleteByPrimaryKey(String id);

    int insert(Attachment record);

    Attachment selectByPrimaryKey(String id);
}
