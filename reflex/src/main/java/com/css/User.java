package com.css;

public class User {

    private String group;
    private double salary;
    private String xAjax;

    public User() {
    }

    public User(String group, double salary, String xAjax) {
        this.group = group;
        this.salary = salary;
        this.xAjax = xAjax;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getxAjax() {
        return xAjax;
    }

    public void setxAjax(String xAjax) {
        this.xAjax = xAjax;
    }

    @Override
    public String toString() {
        return "User{" +
                "group='" + group + '\'' +
                ", salary='" + salary + '\'' +
                ", xAjax='" + xAjax + '\'' +
                '}';
    }

}
