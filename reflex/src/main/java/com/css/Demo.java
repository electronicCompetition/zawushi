package com.css;

import org.testng.annotations.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

public class Demo {

    /**
     * 获取Class对象
     * @throws Exception
     */
    @Test
    public void getClassTest() throws ClassNotFoundException {
        Class<?> clazz = Class.forName("com.css.User");
        Class<? extends User> aClass = User.class;
        Class<User> userClass = User.class;
        System.out.println(clazz == aClass);
        System.out.println(userClass == aClass);
    }

    /**
     * 获取 field
     * @throws NoSuchFieldException
     * @throws IllegalAccessException
     */
    @Test
    public void getDeclaredFieldsTest() throws NoSuchFieldException, IllegalAccessException {
        Field[] declaredFields = User.class.getDeclaredFields();
        System.out.println("*********************************************************");
        Arrays.stream(declaredFields).map(Field::getName).forEach(System.out::println);
        System.out.println("*********************************************************");

        Arrays.stream(declaredFields).forEach(System.out::println);
        System.out.println("*********************************************************");


        Field group = User.class.getDeclaredField("group");
        group.setAccessible(true);
        User u = new User("2", 2.35, "投资二部");
        String value = (String) group.get(u);
        System.out.println(value);

        group.set(u, "douyu");
        System.out.println(u.getGroup());
        System.out.println("*********************************************************");
    }

    /**
     * 获取对象的 Constructor
     * @throws Exception
     */
    @Test
    public void getDeclaredConstructorsTest() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        Constructor<?>[] declaredConstructors = User.class.getDeclaredConstructors();

        System.out.println("*********************************************************");
        Arrays.stream(declaredConstructors).map(Constructor::getName).forEach(System.out::println);
        System.out.println("*********************************************************");

        Constructor<?> declaredConstructor = User.class.getDeclaredConstructor();
        User u = (User) declaredConstructor.newInstance();
        u.setGroup("斗鱼");
        System.out.println(u);
    }

    /**
     * 获取 field
     * @throws NoSuchFieldException
     * @throws IllegalAccessException
     */
    @Test
    public void getDeclaredMethodsTest() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        User u = new User("2", 2.35, "投资二部");
        Method[] declaredMethods = User.class.getDeclaredMethods();

        System.out.println("*********************************************************");
        Arrays.stream(declaredMethods).map(Method::getName).forEach(System.out::println);
        System.out.println("*********************************************************");

        Method getxAjax = User.class.getDeclaredMethod("getxAjax");
        String invoke = (String) getxAjax.invoke(u);
        System.out.println(invoke);
    }


}
